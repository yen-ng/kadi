#!/usr/bin/env python3
# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import subprocess
import sys
from collections import OrderedDict

import click


def _get_max_len(header, requirements, key=None):
    if len(requirements) == 0:
        return len(header)

    if key is None:
        max_value_len = max(len(package) for package in requirements)
    else:
        max_value_len = max(len(item[key]) for item in requirements.values())

    return max(len(header), max_value_len)


@click.command()
def check_requirements():
    """Check all requirements for updates."""
    requirements = OrderedDict()

    # Parse the requirement files to collect all direct dependencies.
    for filename, extra in [("requirements.txt", "-"), ("requirements.dev.txt", "dev")]:
        with open(
            os.path.join(os.path.dirname(__file__), "..", filename), encoding="utf-8"
        ) as f:
            for line in f.readlines():
                requirement = line.split("==")
                requirements[requirement[0]] = {
                    "extra": extra,
                    "current": requirement[1].strip(),
                    "latest": None,
                }

    # Get the latest versions of all outdated packages via pip.
    result = subprocess.run(["pip", "list", "--outdated"], stdout=subprocess.PIPE)
    lines = [line.strip() for line in result.stdout.decode().splitlines()]

    for line in lines[2:]:
        package_meta = line.split()
        package = package_meta[0]

        if package in requirements:
            requirements[package]["latest"] = package_meta[2]

    # Remove all packages that are already up to date.
    for package in list(requirements.keys()):
        requirement_meta = requirements[package]

        if (
            not requirement_meta["latest"]
            or requirement_meta["current"] == requirement_meta["latest"]
        ):
            del requirements[package]

    if len(requirements) == 0:
        click.echo("All requirements are up to date.")
        sys.exit(0)

    # Print all packages and versions.
    package_header = "Package"
    extra_header = "Extra"
    current_ver_header = "Current"
    latest_ver_header = "Latest"

    package_len = _get_max_len(package_header, requirements)
    extra_len = _get_max_len(extra_header, requirements, "extra")
    current_ver_len = _get_max_len(current_ver_header, requirements, "current")
    latest_ver_len = _get_max_len(latest_ver_header, requirements, "latest")

    click.echo(
        f"{package_header.ljust(package_len)}"
        f"  {extra_header.ljust(extra_len)}"
        f"  {current_ver_header.ljust(current_ver_len)}"
        f"  {latest_ver_header.ljust(latest_ver_len)}"
    )
    click.echo(
        f"{'=' * package_len}"
        f"  {'=' * extra_len}"
        f"  {'=' * current_ver_len}"
        f"  {'=' * latest_ver_len}"
    )

    for package, requirement_meta in requirements.items():
        click.echo(
            f"{package.ljust(package_len)}"
            f"  {requirement_meta['extra'].ljust(extra_len)}"
            f"  {requirement_meta['current'].ljust(current_ver_len)}"
            f"  {requirement_meta['latest'].ljust(latest_ver_len)}"
        )


if __name__ == "__main__":
    # pylint: disable=no-value-for-parameter
    check_requirements()
