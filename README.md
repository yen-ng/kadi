# Kadi4Mat

**Kadi4Mat** is the **Karlsruhe Data Infrastructure for Materials Science**, an
open source software for managing research data. More general information about
the project can be found [here](https://kadi.iam-cms.kit.edu).

## Installation

While the packaged code of Kadi4Mat can easily be installed as a Python package
via [pip](https://pypi.org/project/kadi), a complete installation requires a
few additional dependencies and considerations. Please refer to the [stable
documentation](https://kadi4mat.readthedocs.io/en/stable) for full installation
instructions.

## Contributions

Contributions to the code are always welcome. Please refer to the [latest
documentation](https://kadi4mat.readthedocs.io/en/latest) for instructions on
how to set up a development environment of Kadi4Mat as well as other useful
information. In order to merge any contributions back into the main repository,
please open a corresponding [merge
request](https://gitlab.com/iam-cms/kadi/-/merge_requests).

## Issues

For any issues regarding Kadi4Mat (bugs, suggestions, discussions, etc.) please
use the [issue tracker](https://gitlab.com/iam-cms/kadi/-/issues) of this
project. Make sure to add one or more fitting labels to each issue in order to
keep them organized. Before creating an issue, please also check whether a
similar issue is already open first. Note that creating new issues requires a
GitLab account.

For **bugs** in particular, please use the provided `Bug` template when
creating an issue, which also adds the `Bug` label to the issue automatically.
For **security-related** issues or concerns, please prefer
[contacting](https://kadi.iam-cms.kit.edu/#contact) one of the core developers
directly rather than creating a public issue.
