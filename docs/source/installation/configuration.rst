.. _installation-configuration:

Configuration
=============

This section explains the most important configuration values which must or may
need to be set in order to make Kadi4Mat work. The location of the file
containing this configuration and which of the configuration values must be set
or changed depends on the installation method. It is therefore recommended to
have a working (or work in progress) Kadi4Mat :ref:`installation
<installation>` before reading through this section.

Note that the configuration file of Kadi4Mat is a normal Python file. This
means that it always has to be syntactically correct, but it also allows
imports, calculations and all other Python features. For example, it can be
used in combination with some useful constants defined in the Kadi4Mat source
code:

.. code-block:: python3

    import kadi.lib.constants as const
    SOME_KEY = 10 * const.ONE_GB
    # Rest of the config file.

.. raw:: html

    <details>
        <summary>
            <a>kadi/lib/constants.py</a>
        </summary>

.. literalinclude:: ../_links/constants.py
    :language: python3

.. raw:: html

   </details>
   <br>

When changing the configuration file in production environments, it may be
necessary to restart the application and/or dependent services for any changes
to take effect, similar to performing an :ref:`update
<installation-production-updating>`.

Some configuration aspects may also be done via the graphical sysadmin web
interface, which is only available to users marked as sysadmin. To initially
set a user as sysadmin, the Kadi CLI can be used after installing and
configuring Kadi4Mat successfully:

.. code-block:: bash

    sudo su - kadi # Switch to the kadi user (in production environments)
    kadi users sysadmin <user_id>

Authentication
--------------

The configuration concerning authentication is explained separately, as it is a
bit more complex than other configuration values.

.. data:: AUTH_PROVIDERS

    This configuration value specifies the authentication providers to be used
    for logging in to Kadi4Mat. One or more providers can be specified as a
    list of dictionaries, where each dictionary needs to at least contain the
    type of the authentication provider. The following example enables all
    authentication providers that currently exist:

    .. code-block:: python3

        AUTH_PROVIDERS = [
            {"type": "local"},
            {"type": "ldap"},
            {"type": "shib"},
        ]

    Defaults to:

    .. code-block:: python3

        [
            {"type": "local"},
        ]

Each authentication provider also contains various specific configuration
options, which are all left at their default values in the above example. The
following sections describe each provider and its options in more detail. Note
that all of the listed configuration values are the default options of the
respective provider.

Local
~~~~~

Accounts based on local authentication are managed by Kadi4Mat itself.
Therefore, this option requires separate accounts to use Kadi4Mat, but is also
the easiest option to set up and use. Local users can also change their own
password and email address.

.. code-block:: python3

    {
        # The type of the authentication provider.
        "type": "local",
        # The title of the authentication provider that will be shown on the
        # login page.
        "title": "Login with credentials",
        # Whether to allow users that can access the website of Kadi4Mat to
        # register their own accounts by specifying their desired username,
        # display name, email address and password.
        "allow_registration": False,
        # Whether email confirmation through the website of Kadi4Mat is
        # required before any of its features can be used.
        "email_confirmation_required": False,
    }

If not allowing users to register their own accounts, local accounts can be
created manually via the Kadi CLI:

.. code-block:: bash

    sudo su - kadi # Switch to the kadi user (in production environments)
    kadi users create

Sysadmins can also create local users via the graphical sysadmin web interface,
as mentioned above.

LDAP
~~~~

Accounts based on LDAP authentication are managed by a directory service
implementing the LDAP protocol. Therefore, this option allows to use existing
user accounts. LDAP users cannot change their own email address, but may be
able to change their password through Kadi4Mat. Email addresses of LDAP
accounts are confirmed by default.

.. code-block:: python3

    {
        # The type of the authentication provider.
        "type": "ldap",
        # The title of the authentication provider that will be shown on the
        # login page.
        "title": "Login with LDAP",
        # Whether the LDAP server is an Active Directory.
        "active_directory": False,
        # The IP or hostname of the LDAP server.
        "host": "",
        # The port of the the LDAP server to connect with. Besides the default
        # port 389, port 636 is generally used together with SSL.
        "port": 389,
        # The encryption method to use. Can be set to 'ldaps' to use SSL or
        # 'starttls' to use STARTTLS.
        "encryption": None,
        # Whether to validate the server's SSL certificate if an encryption
        # method is set.
        "validate_cert": True,
        # The base DN where users are stored in the LDAP directory, which will
        # be used to perform a simple bind with a user for authentication and
        # to retrieve the additional user attributes after a successful bind.
        # The former works differently depending on whether the server is an
        # Active Directory. For Active Directories, a UserPrincipalName is
        # constructed from the DN's domain components in the form of
        # "<username>@<domain>", where <username> is the specified username
        # when logging in. Otherwise, the full bind DN is constructed as
        # "<username_attr>=<username>,<users_dn>".
        "users_dn": "",
        # The full DN of a user that should be used to perform any LDAP
        # operations after a successful bind. Per default, the bound (i.e.
        # authenticated) user will be used for these operations.
        "bind_user": None,
        # The password of the "bind_user".
        "bind_pw": None,
        # The LDAP attribute name to use for the username, e.g. "uid" or
        # "sAMAccountName". Will also be used for the display name as fallback.
        "username_attr": "uid",
        # The LDAP attribute name to use for the email.
        "email_attr": "mail",
        # The LDAP attribute name to use for the display name. If
        # "firstname_attr" and "lastname_attr" have been specified, these two
        # attributes will take precedence.
        "displayname_attr": "displayName",
        # The LDAP attribute name to use for the first name of the display name
        # instead of "displayname_attr", e.g. "givenName". Must be used
        # together with "lastname_attr".
        "firstname_attr": None,
        # The LDAP attribute name to use for the last name of the display name
        # instead of "displayname_attr", e.g. "sn". Must be used together with
        # "firstname_attr".
        "lastname_attr": None,
        # Whether to allow LDAP users to change their password via Kadi4Mat.
        # This uses the LDAP Password Modify Extended Operation to perform the
        # password change, so keep in mind that any password hashing is done
        # server-side only.
        "allow_password_change": False,
        # Whether to send the old password when changing it, which might be
        # required for some LDAP servers.
        "send_old_password": False,
    }

Shibboleth
~~~~~~~~~~

Accounts based on Shibboleth authentication are managed by one or more
institutions. Therefore, this option allows to use existing user accounts.
Shibboleth users cannot change their own email address or password. Email
addresses of Shibboleth accounts are confirmed by default.

Configuring Shibboleth authentication is currently outside the scope of this
documentation, as it requires additional dependencies, setup and configuration
that also depend on the specific environment and Identity Providers that are to
be used for authentication.

Settings
--------

In this section all configuration values besides authentication are explained.
These include all dependencies, i.e. third party systems, that Kadi4Mat does or
may use as well as other systemically relevant and miscellaneous configuration.

Dependencies
~~~~~~~~~~~~

.. data:: SQLALCHEMY_DATABASE_URI

    The database connection string to use for PostgreSQL in the form of
    ``"postgresql://<user>:<password>@<host>/<database>"``. The database user
    (``<user>``) and database name (``<database>``) will generally be ``kadi``,
    while the password (``<password>``) of the database user is chosen when
    creating the database user. In single-machine setups, the host (``<host>``)
    is usually just ``localhost``. If the used port differs from the default
    value (``5432``), it can be specified after the host as ``<host>:<port>``.

    Defaults to ``None``.

.. data:: ELASTICSEARCH_HOSTS; ELASTICSEARCH_CONFIG

    The connection configuration to use for Elasticsearch.
    ``ELASTICSEARCH_HOSTS`` can either be a single value or a list of values
    specifying one or more Elasticsearch nodes to connect to.
    ``ELASTICSEARCH_CONFIG`` can be used to further customize the connection
    parameters in form of a dictionary. This dictionary will be passed as is to
    the client instance of `elasticsearch-py
    <https://elasticsearch-py.readthedocs.io/en/stable/index.html>`__.

    These values only need to be changed when running Elasticsearch on a
    different machine/port and using TLS/SSL as well as some form of
    authentication, especially when using a more intricate setup than a simple
    single-node deployment.

    Defaults to ``"http://localhost:9200"`` and ``{}``.

.. data:: CELERY_BROKER_URL

    The connection string to use for the Celery broker that communicates
    between Kadi4Mat and the Celery workers. Using Redis is recommended, which
    can be specified in the form of
    ``"redis://<user>:<password>@<host>:<port>/<db>"``.

    This value only needs to be changed when running Redis on a different
    machine/port and using authentication or when using another Redis database.
    It should be safe to use the same Redis connection as for
    ``RATELIMIT_STORAGE_URL``.

    Defaults to ``"redis://localhost:6379/0"``.

.. data:: RATELIMIT_STORAGE_URL

    The connection string to use for caching information used for rate
    limiting. Using Redis is recommended, which can be specified in the form of
    ``"redis://<user>:<password>@<host>:<port>/<db>"``.

    This value only needs to be changed when running Redis on a different
    machine/port and using authentication or when using another Redis database.
    It should be safe to use the same Redis connection as for
    ``CELERY_BROKER_URL``.

    Defaults to ``"redis://localhost:6379/0"``.

.. data:: SENTRY_DSN

    A Sentry DSN (Data Source Name) which can be used to integrate the `Sentry
    <https://sentry.io>`__ error monitoring tool with Kadi4Mat.

    Defaults to ``None``.

Mails
~~~~~

.. data:: SMTP_HOST; SMTP_PORT; SMTP_USERNAME; SMTP_PASSWORD; SMTP_USE_TLS

    These values specify the SMTP connection needed to send emails via
    Kadi4Mat, most importantly the SMTP host (``SMTP_HOST``) and port
    (``SMTP_PORT``). If the SMTP server requires authentication, the username
    and password can be specified using ``SMTP_USERNAME`` and
    ``SMTP_PASSWORD``. If ``SMTP_USE_TLS`` is enabled, STARTTLS will be used
    for an encrypted SMTP connection.

    Defaults to ``"localhost"``, ``25``, ``""``, ``""`` and ``False``.

.. data:: MAIL_NO_REPLY

    The email address that will be used to send no-reply emails from, e.g. for
    confirming email addresses or password reset requests for local accounts.

    Defaults to ``"no-reply@<fqdn>"``, where ``<fqdn>`` is the fully qualified
    domain name of the server.

.. data:: MAIL_ERROR_LOGS

    A list of email addresses that will receive logs of unexpected/uncaught
    errors and exceptions using Python's ``SMTPHandler``. As this will send an
    email for each individual error, setting up Sentry via ``SENTRY_DSN`` may
    be the preferred way for error monitoring.

    Defaults to ``[]``.

Miscellaneous
~~~~~~~~~~~~~

.. data:: EXPERIMENTAL_FEATURES

    Enables or disables experimental features in a Kadi4Mat installation,
    currently only related to workflow functionality.

    Defaults to ``False``.

.. data:: FOOTER_NAV_ITEMS

    Additional navigation items to be listed on the left side of the navigation
    footer. Besides the URL for the navigation item, a title has to be provided
    in one or more languages. The default language (``"en"``) always has to be
    provided as it will also be used as fallback, e.g.:

    .. code-block:: python3

        FOOTER_NAV_ITEMS = [
            ("https://example.com/legals", {"en": "Legals", "de": "Impressum"}),
            ("https://example.com/privacy", {"en": "Privacy policy", "de": "Datenschutz"}),
        ]

    Defaults to ``[]``.

Plugins
~~~~~~~

.. data:: PLUGINS; PLUGIN_CONFIG

    These values are used to enable and configure plugins. See the
    :ref:`Plugins <installation-plugins>` section for more information.

    Defaults to ``[]`` and ``{}``.

Storage
~~~~~~~

.. data:: STORAGE_PATH

    The path that all local files and uploads of records are stored in, e.g.
    ``"/opt/kadi/storage"``. Permissions for this directory should to be set
    accordingly, depending on the user under which the application is running,
    e.g.:

    .. code-block:: bash

        sudo chown kadi:www-data /opt/kadi/storage
        sudo chmod 750 /opt/kadi/storage

    Defaults to ``None``.

.. data:: MISC_UPLOADS_PATH

    The path that all miscellaneous uploads are stored in, like profile or
    group pictures, e.g. ``"/opt/kadi/uploads"``. Permissions for this
    directory should be set accordingly, depending on the user under which the
    application is running, e.g.:

    .. code-block:: bash

        sudo chown kadi:www-data /opt/kadi/uploads
        sudo chmod 750 /opt/kadi/uploads

    Defaults to ``None``.

.. data:: MAX_UPLOAD_SIZE

    The maximum size of a single locally stored file upload in bytes. Setting
    this configuration value to ``None`` will remove the size limit altogether.

    Defaults to ``1000000000`` (1GB).

.. data:: MAX_UPLOAD_USER_QUOTA

    The maximum total size of (active) locally stored files a single user can
    upload. Setting this configuration value to ``None`` will remove the size
    limit altogether.

    Defaults to ``10000000000`` (10GB).

System
~~~~~~

.. data:: SERVER_NAME

    The name or IP of the host that Kadi4Mat runs on, e.g.
    ``"kadi4mat.example.edu"``.

    Defaults to ``None``.

.. data:: SECRET_KEY

    The secret key is used for most things that require encryption or
    signatures in Kadi4Mat, so it should be an appropriately secure value and
    must always be kept secret. The following command can be used to generate a
    suitable value:

    .. code-block:: bash

        tr -dc '[:alnum:]' < /dev/urandom | head -c20

    Defaults to ``None``.

.. data:: PROXY_FIX_ENABLE

    This flag needs to be enabled if Kadi4Mat runs behind an HTTP proxy to
    ensure that e.g. IP addresses reported to the application cannot be spoofed
    by users via HTTP headers. When enabling this flag, ``PROXY_FIX_HEADERS``
    may need to be adjusted accordingly as well.

    Note that when following along with the installation instructions in this
    documentation, this value must **not** be enabled, i.e. everything can be
    left as is in this regard.

    Defaults to ``False``.

.. data:: PROXY_FIX_HEADERS

    These values specify which HTTP headers are set by how many proxies if
    ``PROXY_FIX_ENABLE`` is set. The headers that are taken into account are:

    * ``X-Forwarded-For``, as specified via ``"x_for"``.
    * ``X-Forwarded-Proto``, as specified via ``"x_proto"``.
    * ``X-Forwarded-Host``, as specified via ``"x_host"``.
    * ``X-Forwarded-Port``, as specified via ``"x_port"``.
    * ``X-Forwarded-Prefix``, as specified via ``"x_prefix"``.

    The number after each entry, as seen in the example below, specify how many
    values to trust for each header.

    Defaults to:

    .. code-block:: python3

        {
            "x_for": 1,
            "x_proto": 1,
            "x_host": 1,
            "x_port": 0,
            "x_prefix": 0,
        }
