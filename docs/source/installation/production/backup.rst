.. _installation-production-backup:

Backing up the application
==========================

Aside from backing up the installed application and configuration files itself,
it is important to regularly create backups of the database and locally stored
files in production environments. The following sections highlight some useful
tools and the relevant configuration values of Kadi4Mat in order to get
started.

.. warning::
    As the database and file system used to store local files should be kept in
    sync as much as possible, both backups should always be performed without
    too much time inbetween them. Depending on the amount of data, this might
    require different backup strategies.

Database backups
----------------

As PostgreSQL is used as database system, the ``pg_dump`` utility provided by
PostgreSQL can be used to create backups of the database, which can later be
restored again using ``pg_restore``. The concrete backup command to run depends
on the specific way the backup should be performed and on the installed version
of PostgreSQL.

The following commands show just one example of how to use ``pg_dump``,
including some initial setup, assuming the directory ``/mnt/backups/kadi``
exists and has the correct permissions to be used by the ``kadi`` user:

.. code-block:: bash

    sudo su - kadi                                                          # Switch to the kadi user
    echo "<host>:<port>:<database>:<user>:<password>" > ~/.pgpass           # Create a PostgreSQL password file
    chmod 600 ~/.pgpass                                                     # Give the password file the correct permissions
    pg_dump -Fc -h <host> -U <user> -f /mnt/backups/kadi/db.dump <database> # Perform the actual backup

The PostgreSQL password file and the actual backup command require some
parameters that depend on the ``SQLALCHEMY_DATABASE_URI`` configuration value
as specified in the Kadi configuration file. Please see :ref:`Configuration
<installation-configuration>` for details. For more details about ``pg_dump``
itself, please see the official documentation, e.g. the `following
<https://www.postgresql.org/docs/13/backup-dump.html>`__ for PostgreSQL 13.

When restoring from backup, the search indices may need to be recreated based
on the database contents, unless Elasticsearch is backed up separately. The
following command can be used to perform this task:

.. code-block:: bash

    sudo su - kadi # Switch to the kadi user
    kadi search reindex

In case any old indices still exist, these should still be searchable until the
command finishes (in case the application is already running while performing
the operation). Once the command finishes, the old indices are deleted and
switched with the new ones afterwards. Note that interrupting the command may
lead to orphaned indices being created, which need to be deleted manually, e.g.
by using:

.. code-block:: bash

    sudo su - kadi # Switch to the kadi user
    kadi search remove <index>

For more detailed information, please also refer to the official `Elasticsearch
<https://www.elastic.co/elasticsearch>`__ documentation.

File backups
------------

In order to backup the actual files, namely all locally stored files as well as
miscellaneous uploads (e.g. profile or group pictures), the content stored in
``STORAGE_PATH`` and ``MISC_UPLOADS_PATH`` is relevant. Both configuration
values are again specified in the Kadi configuration file, please see
:ref:`Configuration <installation-configuration>` for details.

When restoring from backup, in order to check the locally stored files for any
inconsistencies in regards to the information stored in the database, the
following command may be used:

.. code-block:: bash

    sudo su - kadi   # Switch to the kadi user
    kadi files check # Check for any inconsistencies
