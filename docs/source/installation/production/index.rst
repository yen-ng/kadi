.. _installation-production:

Production
==========

This section describes how to install, :ref:`update
<installation-production-updating>` and :ref:`backup
<installation-production-backup>` Kadi4Mat in a production environment using a
simple, single-machine setup, which should probably be sufficient already in
many cases.

There are currently two ways to perform the installation: installation via
:ref:`setup script <installation-production-script>` and :ref:`manual
<installation-production-manual>` installation. When using the former, it is
still recommended to check out the manual installation instructions, as all the
necessary steps are described in much more detail, which is also recommended
when planning to deploy a more intricate, multi-machine setup.

.. note::
    After the installation, please make sure to also check out at least the
    :ref:`configuration <installation-configuration>` reference.

.. warning::
    Keep in mind that, for production deployments, there are other aspects to
    consider as well, e.g. securing the server the application and services are
    running on by keeping packages up to date, using a firewall, etc.

.. toctree::
    :maxdepth: 1

    script
    manual
    updating
    backup
