.. _installation-production-manual:

Manual installation
===================

The described installation steps are supposed to be run on a freshly installed
(virtual) machine.

.. note::
    A new system user called ``kadi`` will be created as part of the
    installation. Please make sure that there are no existing users with this
    name on the machine where the installation is being performed.

.. include:: ../_dependencies.rst

uWSGI
~~~~~

`uWSGI <https://uwsgi-docs.readthedocs.io/en/latest>`__ is an application
server implementing the WSGI interface, used to serve the actual Python
application. It can be installed like this:

.. code-block:: bash

    sudo apt install uwsgi uwsgi-plugin-python3

The python3 plugin is needed as well, since the distro-supplied uWSGI package
is built in a modular way.

Apache
~~~~~~

The `Apache HTTP Server <https://httpd.apache.org>`__ is used as a reverse
proxy server in front of uWSGI, handling the actual HTTP requests. The server
as well as all additionally required modules can be installed like this:

.. code-block:: bash

    sudo apt install apache2 libapache2-mod-proxy-uwsgi libapache2-mod-xsendfile

Installing Kadi4Mat
-------------------

Before installing the application, it is best to create a dedicated user that
the application and all required services will run under:

.. code-block:: bash

    sudo adduser kadi --system --home /opt/kadi --ingroup www-data --shell /bin/bash

As some later steps require root privileges again, all commands that require
the newly created user are prefixed with ``kadi $``. Therefore it is best to
switch to the new user using a separate terminal window:

.. code-block:: bash

    sudo su - kadi

To create and activate a new virtual environment for the application, the
following commands can be used:

.. code-block:: bash

    kadi $ virtualenv -p python3 ${HOME}/venv
    kadi $ source ${HOME}/venv/bin/activate

This will create and activate a new virtual environment named *venv* using
Python 3 as interpreter. For all following steps requiring the newly created
user, the virtual environment is assumed to be active. Afterwards, the
application can be installed like this:

.. code-block:: bash

    kadi $ pip install kadi

Configuration
-------------

PostgreSQL
~~~~~~~~~~

To set up PostgreSQL, a user and a database belonging to that user have to be
created. Note that creating the user will prompt for a password, and an
appropriately secure value should be chosen. This password will be used again
later when configuring Kadi4Mat.

.. code-block:: bash

    sudo -u postgres createuser -P kadi
    sudo -u postgres createdb -O kadi kadi -E utf-8

Kadi4Mat
~~~~~~~~

While most of the application configuration values have usable defaults set,
some values need to be set explicitly when using a production environment. For
this, a separate configuration file has to be created, for example:

.. code-block:: bash

    kadi $ mkdir ${HOME}/config
    kadi $ touch ${HOME}/config/kadi.py
    kadi $ chmod 640 ${HOME}/config/kadi.py

This configuration file is important for all services that need access to the
application's configuration, but also for using the Kadi command line interface
(CLI). The Kadi CLI offers some useful tools and utility functions running in
the context of the application (see also :ref:`Command line interfaces
<development-general-cli>`). As such, it also needs access to the configuration
file, which can be done setting the appropriate environment variable:

.. code-block:: bash

    kadi $ export KADI_CONFIG_FILE=${HOME}/config/kadi.py

The above line should also be added to ``.profile`` or a similar configuration
file for convenience, so it is always executed when switching to the new user,
together with activating the virtual environment:

.. code-block:: bash

    kadi $ echo 'export KADI_CONFIG_FILE=${HOME}/config/kadi.py' >> ~/.profile
    kadi $ echo 'test -z "${VIRTUAL_ENV}" && source ${HOME}/venv/bin/activate' >> ~/.profile

Shown below is an example of such a configuration file, listing the most
important configuration options. This example can be used as a starting point
after adjusting some of the sample values, most importantly the *server name*,
*secret key*, *uploads/storage paths* and *database* configuration. Please see
the :ref:`Configuration <installation-configuration>` section for an
explanation of all these options and more general information about the
configuration file.

.. code-block:: python3

    MAIL_NO_REPLY = "no-reply@kadi4mat.example.edu"
    MISC_UPLOADS_PATH = "/opt/kadi/uploads"
    SECRET_KEY = "<secret_key>"
    SERVER_NAME = "kadi4mat.example.edu"
    SMTP_HOST = "localhost"
    SMTP_PASSWORD = ""
    SMTP_PORT = 25
    SMTP_USE_TLS = False
    SMTP_USERNAME = ""
    SQLALCHEMY_DATABASE_URI = "postgresql://kadi:<password>@localhost/kadi"
    STORAGE_PATH = "/opt/kadi/storage"

.. warning::
    Make sure to have a valid :ref:`configuration file
    <installation-configuration>` before continuing with the installation.

uWSGI
~~~~~

To generate a basic configuration for uWSGI, the Kadi CLI can be used:

.. code-block:: bash

    kadi $ kadi utils uwsgi --out kadi.ini

The generated configuration should be rechecked as further customization may be
necessary. Once the configuration is suitable, it can be enabled like this:

.. code-block:: bash

    sudo mv /opt/kadi/kadi.ini /etc/uwsgi/apps-available/
    sudo ln -s /etc/uwsgi/apps-available/kadi.ini /etc/uwsgi/apps-enabled/

As uWSGI serves the actual Kadi4Mat application, potential errors will most
likely end up in the uWSGI log files, unless they occur within a background
task run via celery. All log files of uWSGI can be found at
``/var/log/uwsgi/app``.

Apache
~~~~~~

To generate a basic configuration for Apache, the Kadi CLI can be used:

.. code-block:: bash

    kadi $ kadi utils apache --out kadi.conf

The command will ask, among others, for a certificate and a key file, used to
encrypt the HTTP traffic using SSL/TLS (HTTPS). For testing or internal usage,
a self signed certificate may be used, which can be generated like this (note
that the ``<server_name>`` placeholder has to be substituted with the actual
name or IP of the host that was also configured via the Kadi configuration file
as ``SERVER_NAME``):

.. code-block:: bash

    sudo apt install openssl
    sudo openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout /etc/ssl/private/kadi.key -out /etc/ssl/certs/kadi.crt -subj "/CN=<server_name>" -addext "subjectAltName=DNS:<server_name>"

.. note::
    A proper certificate may also be obtained from `Let's Encrypt
    <https://letsencrypt.org>`__ for free. This is easiest done using their
    `Certbot <https://certbot.eff.org>`__ utility. For this to work, the server
    should already be reachable externally on port 80.

The generated configuration should be rechecked as further customization may be
necessary. Once the configuration is suitable, it can be enabled like this:

.. code-block:: bash

    sudo mv /opt/kadi/kadi.conf /etc/apache2/sites-available/
    sudo a2dissite 000-default
    sudo a2ensite kadi

Finally, all additionally required Apache modules have to be activated as well:

.. code-block:: bash

    sudo a2enmod proxy_uwsgi ssl xsendfile headers

Note that any potential errors regarding Apache can be found inside the log
files at ``/var/log/apache2``.

celery
~~~~~~

To run celery as a background service, it is recommended to set it up as a
systemd service. To generate the necessary unit file, the Kadi CLI can be used
again:

.. code-block:: bash

    kadi $ kadi utils celery --out kadi-celery.service

celery beat, used to execute periodic tasks, needs its own unit file as well:

.. code-block:: bash

    kadi $ kadi utils celerybeat --out kadi-celerybeat.service

The generated configurations should be rechecked as further customization may
be necessary. Once both configurations are suitable, they can be enabled like
this:

.. code-block:: bash

    sudo mv /opt/kadi/kadi-celery.service /etc/systemd/system/
    sudo mv /opt/kadi/kadi-celerybeat.service /etc/systemd/system/

The services need access to the ``/var/log/celery`` directory to store logs,
which has to be created with the appropriate permissions. Note that the user
and group need to be set depending on what values were set in the unit file.

.. code-block:: bash

    sudo mkdir /var/log/celery
    sudo chown -R kadi:www-data /var/log/celery

To store temporary files, access to the ``/run/celery`` directory is needed as
well. Since all directories in ``/run`` are ephemeral, they have to be
recreated on each reboot. To do so, an appropriate configuration has to be
created in ``/usr/lib/tmpfiles.d``. Again, the user and group need to be set
appropriately.

.. code-block:: bash

    echo "d /run/celery 0755 kadi www-data" | sudo tee /usr/lib/tmpfiles.d/celery.conf
    sudo systemd-tmpfiles --create

To let systemd know about the new services and also configure them to start
automatically when the system boots, the following commands can be used:

.. code-block:: bash

    sudo systemctl daemon-reload
    sudo systemctl enable kadi-celery kadi-celerybeat

Additionally, a logrotate config file may be created to make sure that any log
files created by celery are rotated and compressed once a week:

.. code-block:: bash

    echo -e "/var/log/celery/*.log {\n  copytruncate\n  compress\n  delaycompress\n  missingok\n  notifempty\n  rotate 10\n  weekly\n}" | sudo tee /etc/logrotate.d/celery

Setting up the application
--------------------------

Before the application can be used, some initialization steps have to be done
using the Kadi CLI again:

.. code-block:: bash

    kadi $ kadi db init     # Initialize the database
    kadi $ kadi search init # Initialize the search indices

Finally, all new and modified services have to be restarted:

.. code-block:: bash

    sudo systemctl restart apache2 uwsgi kadi-celery kadi-celerybeat

Accessing Kadi4Mat
------------------

Kadi4Mat should now be reachable at ``https://<server_name>``. Again, the
``<server_name>`` placeholder has to be substituted with the actual name or IP
of the host that was also configured via the Kadi configuration file as
``SERVER_NAME``.

To be able to access Kadi4Mat from a different machine, make sure that any
firewall that may run on the server does not block access to ports 80 and 443,
which are the default HTTP(S) ports.
