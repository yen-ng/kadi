.. _installation-production-script:

Installation via setup script
=============================

The setup bash script to install Kadi4Mat can always be found on GitLab, e.g.
the latest stable version is available |install-script-link|. It is supposed to
be run on a freshly installed (virtual) machine. The following commands can be
used to easily run the script from the command line:

.. code-block:: bash
    :substitutions:

    sudo apt install curl
    curl |install-script| > kadi_setup.sh
    less kadi_setup.sh # Let's get an overview about what it does before running it as root
    chmod +x kadi_setup.sh
    sudo ./kadi_setup.sh

.. note::
    The installation script will create a new system user called ``kadi``.
    Please make sure that there are no existing users with this name on the
    machine where the script is executed.

The script should mostly run through autonomously, prompting for input at some
steps where necessary. Once it is complete, some further post-installation
steps may be required, which the script will notify about.
