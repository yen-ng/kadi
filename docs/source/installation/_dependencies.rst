Installing the dependencies
---------------------------

Python and Virtualenv
~~~~~~~~~~~~~~~~~~~~~

As the backend code of the application is based on `Flask
<https://flask.palletsprojects.com>`__ and multiple other Python libraries,
Python needs to be installed. Note that Python >=3.6 is required:

.. code-block:: bash

    sudo apt install python3

To create an isolated environment for the application and its dependencies, it
is highly recommended to use `Virtualenv
<https://virtualenv.pypa.io/en/stable>`__, which can be installed like this:

.. code-block:: bash

    sudo apt install virtualenv

The rest of the documentation assumes Virtualenv is installed.

Libraries
~~~~~~~~~

Some external libraries and tools are required as dependencies for some Python
packages. The following command installs all of them:

.. code-block:: bash

    sudo apt install libmagic1 build-essential python3-dev libpq-dev

PostgreSQL
~~~~~~~~~~

The RDBMS used in the application is `PostgreSQL
<https://www.postgresql.org>`__, which can be installed like this:

.. code-block:: bash

    sudo apt install postgresql

Elasticsearch
~~~~~~~~~~~~~

`Elasticsearch <https://www.elastic.co/elasticsearch>`__ is the full-text
search engine used in the application. Currently, only version 7 is supported,
which can be installed like this:

.. code-block:: bash

    sudo apt install wget apt-transport-https gnupg
    wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
    echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
    sudo apt update && sudo apt install elasticsearch

Some configuration values have to be set manually after installation by using
the following command. These settings configure Elasticsearch to use a
single-node cluster and disable the basic security features included in free
Elasticsearch installations, which are not necessarily needed in this simple
setup.

.. code-block:: bash

    echo -e "discovery.type: single-node\nxpack.security.enabled: false" | sudo tee -a /etc/elasticsearch/elasticsearch.yml

To start Elasticsearch and also configure it to start automatically when the
system boots, the following commands can be used:

.. code-block:: bash

    sudo systemctl enable elasticsearch.service
    sudo systemctl start elasticsearch.service

.. note::
    If the Elasticsearch service has trouble starting in time, try increasing
    the start timeout from 90 seconds to 180 seconds:

    .. code-block:: bash

        sudo mkdir /etc/systemd/system/elasticsearch.service.d
        echo -e "[Service]\nTimeoutStartSec=180" | sudo tee /etc/systemd/system/elasticsearch.service.d/startup-timeout.conf
        sudo systemctl daemon-reload
        sudo systemctl restart elasticsearch.service

Redis
~~~~~

`Redis <https://redis.io>`__ is an in-memory data structure that can be used
for different purposes. Currently it is used as cache for rate limits and as a
message broker for running asynchronous tasks with `celery
<https://docs.celeryproject.org/en/stable>`__, a distributed task queue. It can
be installed like this:

.. code-block:: bash

    sudo apt install redis-server
