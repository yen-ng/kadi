Welcome to Kadi4Mat's documentation!
====================================

|pypi| |license| |zenodo|

.. |pypi| image:: https://img.shields.io/pypi/v/kadi
    :target: https://pypi.org/project/kadi/
    :alt: PyPi

.. |license| image:: https://img.shields.io/pypi/l/kadi
    :target: https://opensource.org/licenses/Apache-2.0
    :alt: License

.. |zenodo| image:: https://zenodo.org/badge/DOI/10.5281/zenodo.4088269.svg
    :target: https://doi.org/10.5281/zenodo.4088269
    :alt: Zenodo

**Kadi4Mat** is the **Karlsruhe Data Infrastructure for Materials Science**, an
open source software for managing research data.

This documentation contains instructions on how to install and configure
Kadi4Mat, some general topics about developing or contributing to Kadi4Mat,
information about the HTTP API Kadi4Mat provides as well as an overview and API
reference of the source code itself. For more information about Kadi4Mat, check
out its `website <https://kadi.iam-cms.kit.edu>`__. The source code of the
project can be found `here <https://gitlab.com/iam-cms/kadi>`__.

.. toctree::
    :name: installation
    :caption: Installation/Updates
    :maxdepth: 1

    installation/production/index
    installation/development/index
    installation/configuration
    installation/plugins

This chapter contains instructions on how to install, update and
:ref:`configure <installation-configuration>` Kadi4Mat as well as its
:ref:`plugins <installation-plugins>`. For regular installations, please refer
to the :ref:`production <installation-production>` instructions. For
development installations, please refer to the :ref:`development
<installation-development>` instructions.

.. note::
    The installation has currently only been tested under **Debian 10** and
    **Ubuntu 20.04**, which the instructions and provided scripts are also
    based on.

.. toctree::
    :name: development
    :caption: Development
    :maxdepth: 1

    development/general
    development/testing
    development/documentation
    development/translations
    development/plugins

This chapter covers various information about developing or contributing to
Kadi4Mat. Before reading this chapter, make sure you have a working
:ref:`development <installation-development>` environment. Some sections assume
that a :ref:`manual <installation-development-manual>` or :ref:`hybrid
<installation-development-hybrid>` development installation is being used,
however, most aspects should also apply to :ref:`Docker
<installation-development-docker>` installations with some variations and
limitations.

.. toctree::
    :name: httpapi
    :caption: HTTP API
    :maxdepth: 1

    httpapi/general
    httpapi/endpoints

This chapter covers the HTTP API provided by Kadi4Mat, namely some
:ref:`general <httpapi-general>` information as well as a list of all available
API :ref:`endpoints <httpapi-endpoints>`. This API makes it possible to
programmatically interact with most of the resources that can be managed via
the graphical user interface of Kadi4Mat by sending suitable HTTP requests to
the different endpoints the API provides.

.. note::
    In parallel to the API itself, a Python wrapper library called **kadi-apy**
    is also under development, which can be used as an easier alternative to
    using the API directly. More information can be found `here
    <https://kadi-apy.readthedocs.io/en/stable>`__.

.. toctree::
    :name: apiref
    :caption: API reference
    :maxdepth: 1

    apiref/project_structure
    apiref/models
    apiref/lib
    apiref/modules
    apiref/plugins

This chapter serves as an API reference for the source code of Kadi4Mat,
currently focused on the backend code, and also gives a brief overview of the
project's general :ref:`structure <apiref-project_structure>`.

.. toctree::
    :name: release-history
    :caption: Release history
    :maxdepth: 2

    _links/HISTORY.md
