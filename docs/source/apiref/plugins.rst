.. _apiref-plugins:

Plugins
=======

This section contains all API references of the :mod:`kadi.plugins` package
that were not already listed in other sections.

.. automodule:: kadi.plugins.core
    :members:
    :show-inheritance:
