.. _apiref-project_structure:

Project structure
=================

This section is intended to give a brief overview about the general structure
of Kadi4Mat's source code. Specifically, the focus lies on everything contained
inside the ``kadi`` directory.

.. _apiref-project_structure-assets:

assets
------

This directory contains all frontend source assets and code, most importantly
JavaScript code, not including smaller scripts written directly in some HTML
templates, stylesheets and frontend translations, found in the ``scripts``,
``styles`` and ``translations`` directories respectively.

Most code inside the ``scripts`` directory is written in plain JavaScript that
is either used globally or only on certain pages. All files using the ``.vue``
file extension contain `Vue.js <https://vuejs.org>`__ components instead, which
are used to encapsulate reusable frontend code. See also :ref:`writing frontend
code <development-general-frontend-writing-code>`.

The ``styles`` directory contains all stylesheets used by the application.
Those can be either normal ``.css`` files or ``.scss`` files using the `Sass
<https://sass-lang.com>`__ extension.

For more details about the translations, see :ref:`Translations
<development-translations>`.

.. _apiref-project_structure-cli:

cli
---

This directory is a Python package that contains code relevant for the command
line interface (CLI). The command line interface provides multiple tools inside
different subcommands that can be utilized by using the ``kadi`` command, which
is available automatically after installing the package. This is done by using
the ``console_scripts`` entry point defined in ``setup.py`` as provided by
setuptools. For building the actual interface, the `Click
<https://click.palletsprojects.com>`__ library is used.

ext
---

This directory is a Python package that contains code relevant for all
extensions used in the application. Most of them are Flask extensions, which
are specifically made to be used inside an existing Flask application. They are
often just wrappers over other libraries, making the integration into a web
application context easier. For most extensions, a global extension instance is
created that can be used throughout the application. Generally, this is made
possible by storing all configuration values needed for an extension to do its
work in the current application object.

lib
---

This directory is a Python package that contains code providing general or
common functionality as well as miscellaneous helper functions.

See also :ref:`Lib <apiref-lib>` for the complete API reference.

.. _apiref-project_structure-migrations:

migrations
----------

This directory contains all scripts and configuration files relevant for
running database schema migrations, see also how to :ref:`adjust or add
database models <development-general-backend-db-models>`.

The ``versions`` directory contains the actual, incremental migration scripts,
ordered by their date of creation. Each script has an upgrade and downgrade
function, to apply or revert changes to a database schema, and potentially
data, respectively.

modules
-------

This directory is a Python package that structures related code and templates
into different *modules*. These are not to be confused with Python modules.

See also :ref:`Modules <apiref-modules>` for the complete API reference.

plugins
-------

This directory is a Python package that contains code used for writing plugins.

See also :ref:`Plugins <apiref-plugins>` for the complete API reference.

static
------

This directory contains static files that can be served directly via a web
server, e.g. scripts and stylesheets as well as images and fonts. Regarding the
former, see also :ref:`writing frontend code
<development-general-frontend-writing-code>`.

templates
---------

This directory contains some globally used HTML templates, snippets and macros,
generally using `Jinja <https://jinja.palletsprojects.com>`__ for server-side
rendering.

translations
------------

This directory contains different files needed for the backend translations.

For more details about the translations, see :ref:`Translations
<development-translations>`.

vendor
------

This directory is a Python package that contains adapted third-party code.
