Miscellaneous
=============

GET
---

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.main.api, kadi.modules.settings.api
    :methods: get
    :autoquickref:
