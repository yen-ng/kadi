Latest (1.0)
============

This section documents the latest version of the HTTP API, currently being
version ``1.0``.

.. toctree::
    :maxdepth: 1

    records
    collections
    groups
    templates
    users
    misc
