Groups
======

GET
---

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.groups.api
    :methods: get
    :autoquickref:

POST
----

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.groups.api
    :methods: post
    :autoquickref:

PATCH
-----

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.groups.api
    :methods: patch
    :autoquickref:

DELETE
------

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.groups.api
    :methods: delete
    :autoquickref:
