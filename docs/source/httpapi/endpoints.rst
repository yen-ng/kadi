.. _httpapi-endpoints:

Endpoints
---------

The following sections consist of the documentation of the actual API endpoints
themselves. The sections are separated by API version, while each section is
itself separated by the different resources that the API provides access to.

.. warning::
    As long as Kadi4Mat is still in beta, all endpoints are considered beta as
    well. Once Kadi4Mat version 1.0+ is released, version 1.0 of the API will
    be considered stable and backwards incompatible changes will be reflected
    in a new API version, if necessary. Until then, endpoints and/or resource
    representations are still subject to change.

The documentation of each endpoint includes its versions, as the implementation
of an endpoint might not change between API versions, required access token
scopes, query parameters and/or request data as well as status codes specific
for the endpoint. If not otherwise noted, the common formats and status codes,
as explained in the :ref:`general <httpapi-general>` information, apply for
each of the documented endpoints. For the representation the returned
resources, please also check out the corresponding :ref:`Models
<apiref-models>`.

.. toctree::
    :maxdepth: 2

    latest/index
