General
=======

.. _development-general-cli:

Command line interfaces
-----------------------

There are two useful command line interfaces (CLI) after installing Kadi4Mat
for running different tools or utility functions. These can be used by running
the following commands:

.. code-block:: bash

    flask # Flask CLI
    kadi  # Kadi CLI

Running them will return a list of all available subcommands. The first CLI
comes from Flask itself (see `Command Line Interface
<https://flask.palletsprojects.com/cli>`__ and also `Working with the Shell
<https://flask.palletsprojects.com/shell>`__) and also includes commands added
by Flask extensions, while the Kadi CLI contains all application-specific
commands. See also :ref:`cli <apiref-project_structure-cli>`.

The Kadi CLI also ensures that each command runs inside the context of the
application, i.e. all commands will have access to the application's
configuration, which can be modified as usual by overriding the default
configuration values, as explained in the :ref:`manual development installation
<installation-development-manual-configuration-kadi4mat>`. For this reason,
some (sub)commands are simply wrappers over existing ones, making their use
easier in certain scenarios or environments.

.. _development-general-tools:

Tools
-----

EditorConfig
~~~~~~~~~~~~

For general editor settings related to indentation, maximum line length and
line endings, the settings in the ``.editorconfig`` file can be applied. This
file can be used in combination with a text editor or IDE that supports it. For
more information, take a look at the `EditorConfig
<https://editorconfig.org>`__ documentation.

.. _development-general-pre-commit:

pre-commit
~~~~~~~~~~

`pre-commit <https://pre-commit.com>`__ is a framework for managing and
maintaining multi-language pre-commit hooks, which get executed each time ``git
commit`` is run. The tool itself should be installed already. The hooks listed
in ``.pre-commit-config.yaml`` can be installed by simply running:

.. code-block:: bash

    pre-commit install

The hooks can also be run manually on all versioned and indexed files using:

.. code-block:: bash

    pre-commit run -a

black
~~~~~

`black <https://black.readthedocs.io/en/stable>`__ is a code formatter which is
used throughout all Python code in the project. The tool itself should be
installed already and can be applied on one or multiple files using:

.. code-block:: bash

    black <path>

Besides running black on the command line, there are also various `integrations
<https://black.readthedocs.io/en/stable/integrations/editors.html>`__ available
for different text editors and IDEs. black is also part of the pre-commit
hooks. As such, it will run automatically on each commit or when running the
pre-commit hooks manually.

Pylint
~~~~~~

`Pylint <https://www.pylint.org>`__ is a static code analysis tool for Python
and should already be installed as well. It can be used on the command line to
aid with detecting some common programming or style mistakes, even if not using
an IDE that already does that. It can be used for the whole ``kadi`` package by
running the following:

.. code-block:: bash

    pylint kadi

Pylint will automatically use the configuration specified in the ``.pylintrc``
file in the application's root directory. Sometimes, there is also code that
should never be checked for certain things. Using specific comments, one can
instruct Pylint to skip such code, e.g. the following line will not raise a
message for an unused import statement:

.. code-block:: python3

    import something # pylint: disable=unused-import

ESLint
~~~~~~

`ESLint <https://eslint.org>`__ is a linter and basic code formatter which is
used for all JavaScript code throughout the project, including any code
snippets inside script tags and Vue.js components. It should be already
installed and can be applied on the whole ``kadi`` folder using:

.. code-block:: bash

    npm run eslint kadi

The configuration of ESlint can be found inside ``.eslintrc.js``. Besides
running ESlint on the command line, there are also various `integrations
<https://eslint.org/docs/user-guide/integrations>`__ available for different
text editors and IDEs. Some files also contain code that should never be
checked for certain things. Using specific comments again, one can instruct
ESLint to skip such code, e.g. the following will suppress errors for unused
variables in the specified function:

.. code-block:: js

    /* eslint-disable no-unused-vars */
    function foo(a) {}
    /* eslint-enable no-unused-vars */

.. code-block:: js

    // eslint-disable-next-line no-unused-vars
    function foo(a) {}

ESLint is also part of the pre-commit hooks. As such, it will run automatically
on each commit or when running the pre-commit hooks manually.

.. _development-general-virtualenvwrapper:

virtualenvwrapper
~~~~~~~~~~~~~~~~~

`virtualenvwrapper <https://virtualenvwrapper.readthedocs.io/en/stable>`__ is
an extension to the Virtualenv tool and can be used to manage and switch
between multiple virtual environments more easily. The tool can be installed
globally, for example by running:

.. code-block:: bash

    sudo apt install virtualenvwrapper

Or by using pip outside any virtual environment, which will generally install a
newer version:

.. code-block:: bash

    pip3 install virtualenvwrapper

Afterwards, some environment variables have to be set. Generally, a suitable
place for them is the ``.bashrc`` file. An example could look like the
following:

.. code-block:: bash

    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    export WORKON_HOME=${HOME}/.venvs
    source ${HOME}/.local/bin/virtualenvwrapper.sh

Please refer to the official documentation about their meaning as well as other
possible variables that can be used, as their values differ by system and
personal preferences.

Backend development
-------------------

Managing dependencies
~~~~~~~~~~~~~~~~~~~~~

All Python dependencies are currently specified via the files
``requirements.txt`` and ``requirements.dev.txt`` (for development
dependencies), which list all direct dependencies used in the project. All
package versions are pinned in order to ensure installations that are mostly
deterministic. In order to check for new package versions, the following helper
script that is included in the Kadi4Mat source code can be used:

.. code-block:: bash

    ${HOME}/workspace/kadi/bin/check_requirements.py

Especially in case of major updates, any updated dependencies should always be
checked for potentially breaking changes beforehand and for any issues that may
arise in the application after the update.

.. _development-general-backend-db-models:

Adjusting or adding database models
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For managing incremental database schema revisions (and potentially data) and
generating corresponding migration scripts, `Alembic
<https://alembic.sqlalchemy.org/en/latest>`__ is used. This way, each change to
the database can be checked into version control in form of the migration
script, which also allows another developer or system administrator to run the
script and get the same incremental updates in their database.

When adding a new model or adjusting an existing one, a new migration script
has to be created to perform the necessary upgrades (see also :ref:`migrations
<apiref-project_structure-migrations>`). To automatically generate such a
script, the following command can be used:

.. code-block:: bash

    flask db migrate -m "Add some new table"

The resulting code of the migration script should be checked and adjusted
accordingly, especially when using custom column types. Afterwards, the
database can be upgraded by running the following command:

.. code-block:: bash

    flask db upgrade

Note that when adjusting existing models further steps may be necessary to
migrate any existing data as well.

Frontend development
--------------------

.. _development-general-frontend-writing-code:

Writing frontend code
~~~~~~~~~~~~~~~~~~~~~

To process and package all frontend assets into individual JavaScript bundles
runnable in a browser context, `webpack <https://webpack.js.org>`__ is used.
The main webpack configuration file can be found in ``webpack.config.js``,
while a few other relevant global settings can be found in
``kadi/assets/scripts/main.js``. When writing frontend code, it is necessary to
run the following in a separate terminal:

.. code-block:: bash

    kadi assets watch

This way, changes to existing files will be detected and the resulting bundles
in ``kadi/static/dist`` will be rebuilt automatically. When adding new files,
the command might have to be restarted to pick them up, depending on which
directory the files reside in. See also :ref:`assets
<apiref-project_structure-assets>`.

Managing dependencies
~~~~~~~~~~~~~~~~~~~~~

All frontend dependencies are managed using `npm <https://www.npmjs.com>`__,
the package manager of `Node.js <https://nodejs.org/en>`__. The tool uses the
dependencies and configuration options as specified in the ``package.json``
file. Additionally, a ``package-lock.json`` file is generated automatically
each time the ``package.json`` file changes (either manually or by installing a
new package) to ensure deterministic installations. In order to check all
dependencies for any updates, the following command can be used:

.. code-block:: bash

    npm outdated

The outdated packages may be shown in different colors, depending on how each
package is specified in ``package.json`` and on the magnitude of the update in
accordance with `Semantic Versioning <https://semver.org>`__. To apply any
updates, one of the the following commands can be used:

.. code-block:: bash

    npm update                  # Automatically update all packages with compatible versions
    npm install <package>@x.y.z # Force the update of a package, e.g. for major version updates

Especially in case of major updates, any updated dependencies should always be
checked for potentially breaking changes beforehand and for any issues that may
arise in the application after the update.

Common issues
-------------

The Flask dev server and/or the webpack watcher are not working properly
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Both the Flask development server and webpack use inotify to efficiently watch
for any file changes to automatically restart the server/rebuild the asset
bundles. The number of file watches that inotify can use may be limited by the
operating system by default. In that case, the limit can be increased
permanently by running:

.. code-block:: bash

    echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p

This amount should be fine in most cases. However, note that this might use
more (unswappable) kernel memory than before.

Code running inside a background task is not updated
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When modifying Python code that is run as part of a background task via celery
and the celery worker itself is already running, it needs to be restarted to
pick up any new changes.

There is currently no builtin way to automatically restart the worker on code
changes. However, the code that is actually executed in the task should
generally be runnable outside of a task as well. Doing so may be more
convenient at first before testing the celery integration.

Can't locate revision identified by <identifier>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This can happen when working on multiple branches with differing commits while
trying to run some database related commands. In this case, one of those
branches is missing one or more database migration scripts/revisions that were
already applied previously. At least one of those scripts should be associated
with the identifier printed in the error message.

The best way to fix it is to simply bring both branches up to date. If this is
not an option for some reason, the database can simply be downgraded again to
the lowest common revision:

.. code-block:: bash

    flask db downgrade <revision>

Note that we need to be one the branch that actually has the missing migration
scripts to be able to run them. Also, this can potentially erase some data from
the database, depending on the content of the migration scripts.
