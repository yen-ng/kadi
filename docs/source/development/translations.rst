.. _development-translations:

Translations
============

Backend
~~~~~~~

Translations in the backend refer to both the backend code itself as well as
server-side rendered templates. For the backend code, the functions offered by
Babel can be used, namely ``gettext`` (usually imported as ``_``),
``lazy_gettext`` (usually imported as ``_l``) and ``ngettext``. Babel is
integrated via the Flask extension `Flask-Babel
<https://flask-babel.tkte.ch>`__.

For server-side rendered templates using Jinja, the same functions can be used.
Additionally, there is the following special directive provided by the i18n
Jinja `extension <https://jinja.palletsprojects.com/templates/#i18n>`__:

.. code-block:: jinja

    {% trans %}text{% endtrans %}

After marking all translatable strings accordingly, the following command can
be used to update all message catalogs (``*.po`` files) found in the
``kadi/translations`` directory:

.. code-block:: bash

    kadi i18n update

After translating all strings, the following command can be used to compile the
message catalogs for use in the application:

.. code-block:: bash

    kadi i18n compile

Frontend
~~~~~~~~

Translations in the frontend refer to both frontend code itself as well as
client-side rendered templates and components. For all frontend translations,
the ``i18next`` library is used, which requires translations in a JSON format.
The existing translations can be found inside ``kadi/assets/translations`` and
also contain the default language, since the library uses keys for each string
to translate, instead of using the strings for the translations directly.

The following example illustrates how a translation can be loaded using a
specified key including interpolation:

.. code-block:: js

    i18n.t('warning.removeIdentifier', {identifier: 'foo'})

Currently, the respective key, which is nested in this case, has to be added
manually to the translation file of the default language. To synchronize the
new key with all other translation files, the following command can be used
instead of doing that step manually as well:

.. code-block:: bash

    kadi i18n sync

This will add any new key from the default translation file to all other
translation files with a placeholder string as translation. Furthermore, all
keys will be sorted alphabetically.
