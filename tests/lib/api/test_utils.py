# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.api.models import AccessToken
from kadi.lib.api.utils import get_api_access_token
from kadi.lib.api.utils import get_api_version
from kadi.lib.api.utils import is_api_request
from kadi.lib.api.utils import is_internal_api_request
from kadi.lib.web import url_for


def test_is_api_request(client, user_session):
    """Test if API requests are detected correctly."""
    with user_session():
        client.get(url_for("main.index"))
        assert not is_api_request()

        client.get(url_for("api.index"))
        assert is_api_request()

        client.get(url_for("api.index_v1_0"))
        assert is_api_request()


def test_is_internal_api_request(client, user_session):
    """Test if internal API requests are detected correctly."""
    with user_session():
        client.get(url_for("api.index"))
        assert not is_internal_api_request()

        client.get(url_for("main.index", _internal=True))
        assert not is_internal_api_request()

        client.get(url_for("api.index", _internal=True))
        assert is_internal_api_request()


def test_get_api_version(client, user_session):
    """Test if API versions are determined correctly."""
    with user_session():
        client.get(url_for("api.index"))
        assert get_api_version() is None

        client.get(url_for("main.index"))
        assert get_api_version() is None

        client.get(url_for("api.index_v1_0"))
        assert get_api_version() == "1.0"


def test_get_api_access_token(api_client, dummy_access_token):
    """Test if the access token is retrieved correctly."""
    assert get_api_access_token() is None

    with api_client() as client:
        client.get(url_for("api.index"))
        assert get_api_access_token() is None

    with api_client(dummy_access_token) as client:
        client.get(url_for("api.index"))
        access_token = get_api_access_token()

        assert access_token
        assert access_token.token_hash == AccessToken.hash_token(dummy_access_token)
