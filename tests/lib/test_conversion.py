# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.conversion import lower
from kadi.lib.conversion import normalize
from kadi.lib.conversion import parse_boolean_string
from kadi.lib.conversion import parse_datetime_string
from kadi.lib.conversion import parse_json_object
from kadi.lib.conversion import recode_string
from kadi.lib.conversion import strip
from kadi.lib.conversion import strip_markdown
from kadi.lib.conversion import truncate


def test_strip():
    """Test if surrounding whitespaces in strings are stripped correctly."""
    assert strip(None) is None
    assert strip(" test ") == "test"
    assert strip([" test ", " foo  bar "]) == ["test", "foo  bar"]


def test_normalize():
    """Test if whitespaces in string are normalized correctly."""
    assert normalize(None) is None
    assert normalize(" foo  bar ") == "foo bar"
    assert normalize([" test ", " foo  bar "]) == ["test", "foo bar"]


def test_lower():
    """Test if strings are lowered correctly."""
    assert lower(None) is None
    assert lower("TEST ") == "test "
    assert lower(["TEST ", "FOO BAR"]) == ["test ", "foo bar"]


def test_truncate():
    """Test if strings are truncated correctly."""
    assert truncate(None, 1) is None
    assert truncate("test", 3) == "tes..."
    assert truncate("test", 4) == "test"
    assert truncate("test", 5) == "test"
    assert truncate(["test", "foo bar"], 4) == ["test", "foo ..."]


def test_recode_string():
    """Test if strings are recoded correctly."""
    assert recode_string(None) is None
    assert recode_string("test", to_encoding="ascii") == "test"
    assert (
        recode_string(b"\xc3\x83\xc2\xb6".decode(), from_encoding="latin-1").encode()
        == b"\xc3\xb6"
    )


def test_parse_datetime_string():
    """Test if datetime strings are parsed correctly."""
    date_time = parse_datetime_string("2020-01-01T12:34:56.000Z")

    assert parse_datetime_string("test") is None
    assert parse_datetime_string("2020-01-01T12:34:56Z") == date_time
    assert parse_datetime_string("2020-01-01T12:34:56+00:00") == date_time
    assert parse_datetime_string("2020-01-01T12:34:56.00000Z") == date_time
    assert parse_datetime_string("2020-01-01T12:34:56.00000+00:00") == date_time


def test_parse_boolean_string():
    """Test if boolean strings are parsed correctly."""
    assert parse_boolean_string("TRUE")
    assert parse_boolean_string("t")
    assert parse_boolean_string("Yes")
    assert parse_boolean_string("Y")
    assert parse_boolean_string("1")
    assert not parse_boolean_string("FALSE")
    assert not parse_boolean_string("test")
    assert not parse_boolean_string("No")
    assert not parse_boolean_string("N")
    assert not parse_boolean_string("0")
    assert not parse_boolean_string(0)
    assert not parse_boolean_string(False)


def test_parse_json_object():
    """Test if JSON object strings are parsed correctly."""
    assert parse_json_object("test") == {}
    assert parse_json_object("true") == {}
    assert parse_json_object('{"test": true}') == {"test": True}


def test_strip_markdown():
    """Test if markdown is stripped correctly from strings."""
    assert strip_markdown("# a") == "a"
    assert strip_markdown("*<b>a</b>*") == "<b>a</b>"
