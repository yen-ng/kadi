# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from uuid import uuid4

import pytest

from kadi.lib.exceptions import KadiValidationError
from kadi.lib.validation import validate_identifier
from kadi.lib.validation import validate_mimetype
from kadi.lib.validation import validate_username
from kadi.lib.validation import validate_uuid


def test_validate_uuid():
    """Test if UUIDs are validated correctly."""
    validate_uuid(str(uuid4()))

    with pytest.raises(KadiValidationError):
        validate_uuid("test")


def test_validate_identifier():
    """Test if identifiers are validated correctly."""
    valid_identifiers = ["-", "_", "a", "0", "0-9_a-_z"]

    for identifier in valid_identifiers:
        validate_identifier(identifier)

    invalid_identifiers = ["", " ", " a", "A", "!"]

    for identifier in invalid_identifiers:
        with pytest.raises(KadiValidationError):
            validate_identifier(identifier)


def test_validate_mimetype():
    """Test if MIME types are validated correctly."""
    validate_mimetype("abc/a-z+0.9")

    invalid_mimetypes = ["", " ", "/a", "0/a", "a/A", "a/!"]

    for mimetype in invalid_mimetypes:
        with pytest.raises(KadiValidationError):
            validate_mimetype(mimetype)


def test_validate_username():
    """Test if local usernames are validated correctly."""
    valid_usernames = ["a", "0", "a-z-0-9"]

    for username in valid_usernames:
        validate_username(username)

    invalid_usernames = ["", " ", " a", "A", "!", "-", "a-", "-a", "a--0"]

    for username in invalid_usernames:
        with pytest.raises(KadiValidationError):
            validate_username(username)
