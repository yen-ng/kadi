# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.licenses.models import License
from kadi.modules.templates.forms import NewExtrasTemplateForm
from kadi.modules.templates.forms import NewRecordTemplateForm


def test_new_record_template_form(dummy_template, dummy_record, dummy_user):
    """Test if prefilling a "NewRecordTemplateForm" with a template works correctly."""
    license = License.create(name="name", title="title")
    dummy_template.data = {
        "type": "test",
        "license": "name",
        "tags": ["test"],
        "extras": [],
    }

    form = NewRecordTemplateForm(template=dummy_template, user=dummy_user)

    type = dummy_template.data["type"]
    tag = dummy_template.data["tags"][0]

    assert form.title.data == dummy_template.title
    assert form.record_type.initial == (type, type)
    assert form.record_license.initial == (license.name, license.title)
    assert form.record_tags.initial == [(tag, tag)]
    assert form.record_extras.data == dummy_template.data["extras"]


def test_new_extras_template_form_template(dummy_user, new_template):
    """Test if prefilling a "NewExtrasTemplateForm" with a template works correctly."""
    template = new_template(
        type="extras", data=[{"type": "str", "key": "test", "value": "test"}]
    )
    form = NewExtrasTemplateForm(template=template, user=dummy_user)

    assert form.title.data == template.title
    assert form.extras.data == template.data


def test_new_extras_template_form_record(dummy_record, dummy_user):
    """Test if prefilling a "NewExtrasTemplateForm" with a record works correctly."""
    dummy_record.extras = [
        {"type": "str", "key": "test", "value": "test"},
        {
            "type": "dict",
            "key": "test2",
            "value": [{"type": "str", "key": "test", "value": "test"}],
        },
    ]
    form = NewExtrasTemplateForm(record=dummy_record, user=dummy_user)

    assert form.extras.data == [
        {"type": "str", "key": "test", "value": None},
        {
            "type": "dict",
            "key": "test2",
            "value": [{"type": "str", "key": "test", "value": None}],
        },
    ]
