# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.resources.utils import add_link
from kadi.modules.collections.forms import NewCollectionForm


def test_new_collection_form(
    dummy_collection, dummy_record, dummy_user, new_record, new_user
):
    """Test if prefilling a "NewCollectionForm" with a collection works correctly."""
    tag = "test"
    dummy_collection.set_tags([tag])
    add_link(dummy_collection.records, dummy_record, user=dummy_user)

    user = new_user()
    # This record should not appear in the linked records in the form.
    record = new_record(creator=user)
    add_link(dummy_collection.records, record, user=user)

    form = NewCollectionForm(collection=dummy_collection, user=dummy_user)

    assert form.title.data == dummy_collection.title
    assert form.tags.initial == [(tag, tag)]
    assert form.linked_records.initial == [
        (dummy_record.id, f"@{dummy_record.identifier}")
    ]
    assert form.copy_permission.initial == (
        dummy_collection.id,
        f"@{dummy_collection.identifier}",
    )
