# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json

from kadi.lib.resources.utils import add_link
from kadi.modules.collections.export import get_export_data


def test_get_export_data_json(dummy_collection, dummy_record, dummy_user):
    """Test if the collection JSON export works correctly."""
    add_link(dummy_collection.records, dummy_record, user=dummy_user)

    export_filter = {"user": True}
    json_data = get_export_data(
        dummy_collection, "json", export_filter=export_filter, user=dummy_user
    )
    data = json.loads(json_data)

    assert data is not None
    assert data.get("creator") is None
    assert data["records"][0].get("creator") is None


def test_get_export_data_qr(dummy_collection, dummy_user):
    """Test if the collection QR code export works correctly."""
    assert get_export_data(dummy_collection, "qr", user=dummy_user).getvalue()
