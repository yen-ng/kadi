# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app

from kadi.lib.web import make_next_url
from kadi.lib.web import url_for
from kadi.modules.accounts.models import User
from kadi.modules.groups.models import Group
from kadi.modules.permissions.core import add_role
from kadi.modules.permissions.models import Permission
from kadi.modules.permissions.models import Role
from kadi.modules.permissions.utils import get_group_roles
from kadi.modules.permissions.utils import get_user_roles
from kadi.modules.permissions.utils import initialize_system_role
from tests.utils import check_api_response
from tests.utils import check_view_response


def test_permission_required_api(api_client, dummy_record, new_access_token, new_user):
    """Test the "permission_required" decorator for API requests."""
    user = new_user()
    token = new_access_token(user=user)

    # Unauthorized request.
    response = api_client().get(url_for("api.get_record", id=dummy_record.id))
    check_api_response(response, status_code=401)

    # Unpermitted request.
    response = api_client(token).get(url_for("api.get_record", id=dummy_record.id))
    check_api_response(response, status_code=403)

    # Permitted request.
    add_role(user, "record", dummy_record.id, "member")

    response = api_client(token).get(url_for("api.get_record", id=dummy_record.id))
    check_api_response(response)

    # Invalid object ID.
    response = api_client(token).get(url_for("api.get_record", id=dummy_record.id + 1))
    check_api_response(response, status_code=404)


def test_permission_required_views(client, dummy_record, new_user, user_session):
    """Test the "permission_required" decorator for non-API requests."""
    username = password = "test"
    user = new_user(username=username, password=password)

    # Unauthorized request.
    endpoint = url_for("records.view_record", id=dummy_record.id)
    response = client.get(endpoint)

    check_view_response(response, status_code=302)
    assert response.location == make_next_url(endpoint)

    with user_session(username=username, password=password):
        # Unpermitted request.
        response = client.get(url_for("records.view_record", id=dummy_record.id))
        check_view_response(response, status_code=403)

        # Permitted request.
        add_role(user, "record", dummy_record.id, "member")

        response = client.get(url_for("records.view_record", id=dummy_record.id))
        check_view_response(response)

        # Invalid object ID.
        response = client.get(url_for("records.view_record", id=dummy_record.id + 1))
        check_view_response(response, status_code=404)


def test_initialize_system_role(new_user):
    """Test if system roles are initialized correctly."""
    for role_name in current_app.config["SYSTEM_ROLES"]:
        # The system roles should already be initialized via the database fixtures.
        assert not initialize_system_role(role_name)

    for name, permissions in current_app.config["SYSTEM_ROLES"].items():
        role = Role.query.filter_by(name=name, object=None, object_id=None).one()

        for object_name, actions in permissions.items():
            for action in actions:
                permission = Permission.query.filter_by(
                    action=action, object=object_name, object_id=None
                ).one()

                assert permission in role.permissions


def test_get_user_roles(dummy_record, dummy_user):
    """Test if determining user roles works correctly."""
    user_role = get_user_roles("record").one()

    assert isinstance(user_role[0], User)
    assert isinstance(user_role[1], Role)

    assert user_role[0].id == dummy_user.id
    assert user_role[1].object_id == dummy_record.id

    user_role = get_user_roles("record", object_id=dummy_record.id).one()

    assert user_role[0].id == dummy_user.id
    assert user_role[1].object_id == dummy_record.id


def test_get_group_roles(dummy_group, dummy_record):
    """Test if determining group roles works correctly."""
    add_role(dummy_group, "record", dummy_record.id, "member")
    group_role = get_group_roles("record").one()

    assert isinstance(group_role[0], Group)
    assert isinstance(group_role[1], Role)

    assert group_role[0].id == dummy_group.id
    assert group_role[1].object_id == dummy_record.id

    group_role = get_group_roles("record", object_id=dummy_record.id).one()

    assert group_role[0].id == dummy_group.id
    assert group_role[1].object_id == dummy_record.id
