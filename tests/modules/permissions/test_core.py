# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.modules.permissions.core import add_role
from kadi.modules.permissions.core import delete_permissions
from kadi.modules.permissions.core import get_permitted_objects
from kadi.modules.permissions.core import has_permission
from kadi.modules.permissions.core import remove_role
from kadi.modules.permissions.core import set_system_role
from kadi.modules.permissions.models import Permission
from kadi.modules.permissions.models import Role


def test_has_permission(dummy_group, dummy_record, dummy_user, new_record, new_user):
    """Test if permissions are determined correctly."""
    action = "read"
    object_name = "record"
    role = "member"

    # Test with invalid parameters.
    assert not has_permission(dummy_user, "invalid", object_name, dummy_record.id)
    assert not has_permission(dummy_user, action, "invalid", dummy_record.id)
    assert not has_permission(dummy_user, action, object_name, dummy_record.id + 1)

    # Test global actions.
    user = new_user()
    user.permissions.append(
        Permission.query.filter_by(action="read", object="record", object_id=None).one()
    )
    assert not has_permission(dummy_user, action, object_name, None)
    assert has_permission(user, action, object_name, None)
    assert has_permission(user, action, object_name, dummy_record.id)

    # Test default permissions.
    record = new_record(creator=new_user(), visibility="public")

    assert has_permission(dummy_user, action, object_name, record.id)
    assert not has_permission(
        dummy_user, action, object_name, record.id, check_defaults=False
    )

    # Test invalid permissions.
    user = new_user()
    record = new_record(creator=user)

    assert not has_permission(dummy_user, action, object_name, record.id)
    assert not has_permission(dummy_group, action, object_name, dummy_record.id)
    assert not has_permission(user, action, object_name, dummy_record.id)

    # Test valid permissions with and without group permission checks.
    user_1 = new_user()

    add_role(user_1, "group", dummy_group.id, role)
    add_role(dummy_group, object_name, dummy_record.id, role)

    user_2 = new_user()
    user_2.permissions.append(
        Permission.query.filter_by(
            action="read", object="record", object_id=dummy_record.id
        ).one()
    )

    assert has_permission(dummy_user, action, object_name, dummy_record.id)
    assert has_permission(dummy_group, action, object_name, dummy_record.id)
    assert has_permission(user_1, action, object_name, dummy_record.id)
    assert has_permission(user_2, action, object_name, dummy_record.id)

    assert has_permission(
        dummy_user, action, object_name, dummy_record.id, check_groups=False
    )
    assert has_permission(
        dummy_group, action, object_name, dummy_record.id, check_groups=False
    )
    assert not has_permission(
        user_1, action, object_name, dummy_record.id, check_groups=False
    )
    assert has_permission(
        user_2, action, object_name, dummy_record.id, check_groups=False
    )


def test_get_permitted_objects(
    dummy_group, dummy_record, dummy_user, new_group, new_record, new_user
):
    """Test if permitted objects are determined correctly."""
    action = "read"
    object_name = "record"
    role = "member"

    # Test with invalid parameters.
    assert not get_permitted_objects(dummy_user, "invalid", object_name).all()
    assert get_permitted_objects(dummy_user, action, "invalid") is None

    # Test global actions.
    user = new_user()
    user.permissions.append(
        Permission.query.filter_by(action="read", object="record", object_id=None).one()
    )
    assert get_permitted_objects(user, action, object_name).one().id == dummy_record.id

    # Test default permissions.
    record = new_record(creator=new_user(), visibility="public")

    assert get_permitted_objects(dummy_user, action, object_name).count() == 2
    assert (
        get_permitted_objects(dummy_user, action, object_name, check_defaults=False)
        .one()
        .id
        == dummy_record.id
    )

    record.visibility = "private"

    # Test valid permissions with and without group permission checks.
    user_1 = new_user()

    add_role(user_1, "group", dummy_group.id, role)
    add_role(dummy_group, object_name, dummy_record.id, role)

    user_2 = new_user()
    user_2.permissions.append(
        Permission.query.filter_by(
            action="read", object="record", object_id=dummy_record.id
        ).one()
    )

    assert (
        get_permitted_objects(dummy_user, action, object_name).one().id
        == dummy_record.id
    )
    assert (
        get_permitted_objects(dummy_group, action, object_name).one().id
        == dummy_record.id
    )
    assert (
        get_permitted_objects(user_1, action, object_name).one().id == dummy_record.id
    )
    assert (
        get_permitted_objects(user_2, action, object_name).one().id == dummy_record.id
    )

    assert (
        get_permitted_objects(dummy_user, action, object_name, check_groups=False)
        .one()
        .id
        == dummy_record.id
    )
    assert (
        get_permitted_objects(dummy_group, action, object_name, check_groups=False)
        .one()
        .id
        == dummy_record.id
    )
    assert not get_permitted_objects(
        user_1, action, object_name, check_groups=False
    ).all()
    assert (
        get_permitted_objects(user_2, action, object_name, check_groups=False).one().id
        == dummy_record.id
    )


def test_add_role(dummy_group, dummy_record, new_user):
    """Test if adding roles works correctly."""
    user = new_user()
    role = "member"
    prev_timestamp = dummy_record.last_modified

    with pytest.raises(ValueError) as e:
        add_role(user, "record", dummy_record.id + 1, role)
        assert (
            str(e) == f"Object 'record' with ID {dummy_record.id + 1} does not exist."
        )

    with pytest.raises(ValueError) as e:
        # Test adding the role to itself, even though group roles are not currently
        # implemented for groups.
        add_role(dummy_group, "group", dummy_group.id, role)
        assert str(e) == "Cannot add a role to the object to which the role refers."

    with pytest.raises(ValueError) as e:
        add_role(user, "record", dummy_record.id, "invalid")
        assert str(e) == "A role with that name does not exist."

    assert add_role(user, "record", dummy_record.id, role)
    assert not add_role(user, "record", dummy_record.id, role)
    assert user.roles.filter(
        Role.object == "record", Role.object_id == dummy_record.id, Role.name == role
    ).one()
    assert dummy_record.last_modified != prev_timestamp


def test_remove_role(dummy_record, new_user):
    """Test if removing roles works correctly."""
    user = new_user()
    role = "member"
    prev_timestamp = dummy_record.last_modified

    with pytest.raises(ValueError) as e:
        remove_role(user, "record", dummy_record.id + 1, role)
        assert (
            str(e) == f"Object 'record' with ID {dummy_record.id + 1} does not exist."
        )

    assert not remove_role(user, "record", dummy_record.id, role)

    add_role(user, "record", dummy_record.id, role)

    assert remove_role(user, "record", dummy_record.id, role)
    assert (
        user.roles.filter(
            Role.object == "record", Role.object_id == dummy_record.id
        ).first()
        is None
    )
    assert dummy_record.last_modified != prev_timestamp


def test_set_system_role(dummy_user):
    """Test if system roles are set correctly."""
    role_query = dummy_user.roles.filter(
        Role.object.is_(None), Role.object_id.is_(None)
    )

    assert not set_system_role(dummy_user, "test")
    assert role_query.one().name == "member"

    assert set_system_role(dummy_user, "admin")
    assert role_query.one().name == "admin"


@pytest.mark.parametrize(
    "fixture", ["dummy_record", "dummy_collection", "dummy_group", "dummy_template"]
)
def test_setup_permissions(fixture, getfixture, dummy_user):
    """Test if the default permissions of created resources are set up correctly."""
    resource = getfixture(fixture)
    model = resource.__class__
    object_name = model.__tablename__

    for name, actions in model.Meta.permissions["roles"]:
        role = Role.query.filter_by(
            name=name, object=object_name, object_id=resource.id
        ).one()

        for action in actions:
            permission = Permission.query.filter_by(
                action=action, object=object_name, object_id=resource.id
            ).one()

            assert permission in role.permissions


def test_delete_permissions(db, dummy_record):
    """Test if deleting permissions works correctly."""
    delete_permissions("record", dummy_record.id)
    db.session.commit()

    assert not Permission.query.filter_by(
        object="record", object_id=dummy_record.id
    ).all()
    assert not Role.query.filter_by(object="record", object_id=dummy_record.id).all()
