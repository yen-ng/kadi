# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.modules.permissions.core import add_role
from kadi.modules.records.models import RecordLink
from kadi.modules.records.utils import get_permitted_record_links
from kadi.modules.records.utils import get_record_links_graph


def test_get_permitted_record_links(dummy_record, dummy_user, new_record, new_user):
    """Test if permitted record links are determined correctly."""
    user = new_user()
    record = new_record()

    # Link the new record with the dummy record and give the new user only access to the
    # dummy record.
    link = RecordLink.create(name="test", record_from=dummy_record, record_to=record)
    add_role(user, "record", dummy_record.id, "member")

    assert (
        get_permitted_record_links(dummy_record, direction="to", user=dummy_user)
        .one()
        .id
        == link.id
    )
    assert not get_permitted_record_links(
        dummy_record, direction="from", user=dummy_user
    ).all()
    assert not get_permitted_record_links(dummy_record, user=user).all()


def test_get_record_links_graph(dummy_record, dummy_user, new_record):
    """Test if record link graph data is generated correctly."""
    record = new_record()
    link = RecordLink.create(name="test", record_from=dummy_record, record_to=record)

    data = get_record_links_graph(dummy_record, user=dummy_user)

    assert len(data["nodes"]) == 2
    assert data["links"] == [
        {
            "source": dummy_record.id,
            "target": record.id,
            "name": link.name,
            "name_full": link.name,
            "link_index": 1,
            "link_length": len(link.name),
        }
    ]
