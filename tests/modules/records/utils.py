# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import hashlib
from io import BytesIO


def initiate_upload(
    client,
    endpoint,
    file_data=b"",
    name="test.txt",
    mimetype="text/plain",
    size=None,
    checksum=None,
    replaces_file=False,
):
    """Initiate a new upload or an upload to replace an existing file."""
    size = size if size is not None else len(file_data)
    checksum = checksum if checksum is not None else hashlib.md5(file_data).hexdigest()

    payload = {"mimetype": mimetype, "size": size, "checksum": checksum}

    if replaces_file:
        response = client.put(endpoint, json=payload)
    else:
        payload["name"] = name
        response = client.post(endpoint, json=payload)

    return response


def upload_chunk(client, endpoint, chunk_data=b"", index=0, size=None, checksum=None):
    """Upload a chunk of an existing upload."""
    size = size if size is not None else len(chunk_data)
    checksum = checksum if checksum is not None else hashlib.md5(chunk_data).hexdigest()

    response = client.put(
        endpoint,
        data={
            "blob": (BytesIO(chunk_data), f"chunk_{index}"),
            "index": str(index),
            "size": str(size),
            "checksum": checksum,
        },
        content_type="multipart/form-data",
    )

    return response
