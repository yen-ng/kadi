# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json

from kadi.lib.resources.utils import add_link
from kadi.modules.records.export import get_export_data
from kadi.modules.records.models import RecordLink


def test_get_export_data_json(dummy_collection, dummy_record, dummy_user, new_record):
    """Test if the record JSON export works correctly."""
    dummy_record.extras = [{"key": "test", "type": "str", "value": None}]
    record = new_record()
    RecordLink.create(name="test", record_from=dummy_record, record_to=record)
    add_link(dummy_record.collections, dummy_collection, user=dummy_user)

    export_filter = {
        "user": True,
        "collections": True,
        "links": True,
        "extras": {"test": {}},
    }
    json_data = get_export_data(
        dummy_record, "json", export_filter=export_filter, user=dummy_user
    )
    data = json.loads(json_data)

    assert data is not None
    assert data.get("creator") is None
    assert data.get("collections") is None
    assert data.get("links") is None
    assert data["extras"] == []


def test_record_pdf(dummy_record, dummy_user):
    """Test if the record PDF export works correctly."""

    # Check handling of unicode characters and some extra metadata with a date.
    dummy_record.description = b"\xf0\x9f\x90\xb1".decode()
    dummy_record.extras = [
        {
            "key": "test",
            "type": "dict",
            "value": [
                {
                    "key": "test",
                    "type": "date",
                    "value": "2020-01-01T12:34:56.789000+00:00",
                }
            ],
        }
    ]

    assert get_export_data(dummy_record, "pdf", user=dummy_user).getvalue()


def test_get_export_data_qr(dummy_record, dummy_user):
    """Test if the record QR code export works correctly."""
    assert get_export_data(dummy_record, "qr", user=dummy_user).getvalue()
