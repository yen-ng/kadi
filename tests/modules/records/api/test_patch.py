# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from tests.modules.utils import check_api_patch_subject_resource_role
from tests.utils import check_api_response


def test_edit_record(api_client, dummy_access_token, dummy_record):
    """Test the "api.edit_record" endpoint."""
    response = api_client(dummy_access_token).patch(
        url_for("api.edit_record", id=dummy_record.id), json={"identifier": "test"}
    )

    check_api_response(response)
    assert dummy_record.identifier == "test"


def test_change_record_user_role(
    api_client, dummy_access_token, dummy_record, dummy_user, new_user
):
    """Test the "api.change_record_user_role" endpoint."""
    user = new_user()

    check_api_patch_subject_resource_role(
        api_client(dummy_access_token),
        url_for(
            "api.change_record_user_role", record_id=dummy_record.id, user_id=user.id
        ),
        user,
        dummy_record,
        change_creator_endpoint=url_for(
            "api.change_record_user_role",
            record_id=dummy_record.id,
            user_id=dummy_user.id,
        ),
    )


def test_change_record_group_role(
    api_client, dummy_access_token, dummy_record, dummy_group
):
    """Test the "api.change_record_group_role" endpoint."""
    check_api_patch_subject_resource_role(
        api_client(dummy_access_token),
        url_for(
            "api.change_record_group_role",
            record_id=dummy_record.id,
            group_id=dummy_group.id,
        ),
        dummy_group,
        dummy_record,
    )


def test_edit_file_metadata(api_client, dummy_access_token, dummy_file, dummy_record):
    """Test the "api.edit_file_metadata" endpoint."""
    new_name = "test"

    response = api_client(dummy_access_token).patch(
        url_for(
            "api.edit_file_metadata", record_id=dummy_record.id, file_id=dummy_file.id
        ),
        json={"name": new_name},
    )

    check_api_response(response)
    assert dummy_file.name == new_name
