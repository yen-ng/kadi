# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app

from kadi.lib.web import url_for
from kadi.modules.records.previews import get_preview_data


def test_get_preview_data(monkeypatch, tmp_path, dummy_file, new_file):
    """Test some of the builtin preview types."""
    monkeypatch.setitem(current_app.config, "STORAGE_PATH", tmp_path)

    text_file = new_file(file_data=b"test\r\n")
    csv_file = new_file(file_data=b"f;o;o\r\n;;\n\n", mimetype="text/csv")

    assert get_preview_data(dummy_file) == (
        "image",
        url_for(
            "api.preview_file", record_id=dummy_file.record_id, file_id=dummy_file.id
        ),
    )
    assert get_preview_data(text_file) == (
        "text",
        {"lines": ["test"], "encoding": "ascii"},
    )
    assert get_preview_data(csv_file) == (
        "csv",
        {
            "rows": [["f", "o", "o"], ["", "", ""]],
            "encoding": "ascii",
            "has_header": True,
        },
    )
