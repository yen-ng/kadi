# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import json
import math

import pytest
from marshmallow import ValidationError
from werkzeug.datastructures import MultiDict

from kadi.lib.conversion import parse_datetime_string
from kadi.lib.forms import KadiForm
from kadi.modules.records.extras import ExtraSchema
from kadi.modules.records.extras import ExtrasField
from kadi.modules.records.extras import MAX_INTEGER


def test_extras_jsonb(dummy_record):
    """Test if the custom JSONB type for extras works correctly."""
    dummy_record.extras = [{"type": "float", "key": "test", "value": 1e100}]
    assert isinstance(dummy_record.extras[0]["value"], float)


@pytest.mark.parametrize(
    "data,extra,is_template",
    [
        (
            {"type": " str ", "key": " test "},
            {"type": "str", "key": "test", "value": None},
            False,
        ),
        (
            {"type": "str", "key": "test", "value": None},
            None,
            False,
        ),
        (
            {"type": "str", "key": "test", "value": " test "},
            {"type": "str", "key": "test", "value": "test"},
            False,
        ),
        (
            {"type": "str", "key": "test", "value": " ", "unit": None},
            {"type": "str", "key": "test", "value": None},
            False,
        ),
        (
            {"type": "int", "key": "test", "value": " "},
            {"type": "int", "key": "test", "value": None, "unit": None},
            False,
        ),
        (
            {"type": "int", "key": "test", "value": 123},
            {"type": "int", "key": "test", "value": 123, "unit": None},
            False,
        ),
        (
            {"type": "float", "key": "test", "value": -123, "unit": None},
            None,
            False,
        ),
        (
            {"type": "float", "key": "test", "value": 1e123, "unit": " cm "},
            {"type": "float", "key": "test", "value": 1e123, "unit": "cm"},
            False,
        ),
        (
            {"type": "date", "key": "test", "value": "2020-01-01T12:34:56.789Z"},
            {
                "type": "date",
                "key": "test",
                "value": "2020-01-01T12:34:56.789000+00:00",
            },
            False,
        ),
        (
            {
                "type": "date",
                "key": "test",
                "value": parse_datetime_string("2020-01-01T12:34:56.789Z"),
            },
            {
                "type": "date",
                "key": "test",
                "value": "2020-01-01T12:34:56.789000+00:00",
            },
            False,
        ),
        (
            {"type": "bool", "key": "test", "value": True},
            None,
            False,
        ),
        (
            {"type": "dict", "key": "test"},
            {"type": "dict", "key": "test", "value": []},
            False,
        ),
        (
            {
                "type": "dict",
                "key": "test",
                "value": [{"type": "str", "key": "test", "value": "test"}],
            },
            None,
            False,
        ),
        (
            {
                "type": "list",
                "key": "test",
                "value": [{"type": "str", "value": "test"}],
            },
            None,
            False,
        ),
        (
            {"type": "str", "key": "test", "value": "test", "validation": {}},
            {"type": "str", "key": "test", "value": "test"},
            False,
        ),
        (
            {
                "type": "str",
                "key": "test",
                "value": "test",
                "validation": {"required": True},
            },
            None,
            False,
        ),
        (
            {
                "type": "str",
                "key": "test",
                "value": None,
                "validation": {"required": True},
            },
            None,
            True,
        ),
        (
            {
                "type": "dict",
                "key": "test",
                "value": [
                    {
                        "type": "str",
                        "key": "test",
                        "value": None,
                        "validation": {"required": True},
                    }
                ],
            },
            None,
            True,
        ),
        (
            {
                "type": "str",
                "key": "test",
                "value": None,
                "validation": {"options": ["test", "test"]},
            },
            {
                "type": "str",
                "key": "test",
                "value": None,
                "validation": {"options": ["test"]},
            },
            False,
        ),
        (
            {
                "type": "str",
                "key": "test",
                "value": "test",
                "validation": {"required": True, "options": ["test"]},
            },
            None,
            False,
        ),
    ],
)
def test_extra_schema_success(data, extra, is_template):
    """Test if the extra schema validates valid extras correctly."""
    if extra is None:
        extra = data

    assert ExtraSchema(is_template=is_template).load(data) == extra


@pytest.mark.parametrize(
    "data,is_template",
    [
        (
            {},
            False,
        ),
        (
            {"type": "str"},
            False,
        ),
        (
            {"type": "str", "key": " "},
            False,
        ),
        (
            {"type": "test", "key": "test"},
            False,
        ),
        (
            {"type": "str", "key": "test", "unit": "test"},
            False,
        ),
        (
            {"type": "str", "key": "test", "value": 123},
            False,
        ),
        (
            {"type": "int", "key": "test", "value": "123"},
            False,
        ),
        (
            {"type": "int", "key": "test", "value": 123.0},
            False,
        ),
        (
            {"type": "int", "key": "test", "value": MAX_INTEGER + 1},
            False,
        ),
        (
            {"type": "int", "key": "test", "value": -(MAX_INTEGER + 1)},
            False,
        ),
        (
            {"type": "float", "key": "test", "value": math.inf},
            False,
        ),
        (
            {"type": "float", "key": "test", "value": math.nan},
            False,
        ),
        (
            {"type": "bool", "key": "test", "value": "true"},
            False,
        ),
        (
            {"type": "date", "key": "test", "value": "test"},
            False,
        ),
        (
            {"type": "dict", "key": "test", "value": {}},
            False,
        ),
        (
            {
                "type": "dict",
                "key": "test",
                "value": [
                    {"type": "str", "key": "test"},
                    {"type": "str", "key": "test"},
                ],
            },
            False,
        ),
        (
            {"type": "list", "key": "test", "value": [{"type": "str", "key": "test"}]},
            False,
        ),
        (
            {"type": "str", "key": "test", "validation": {"required": True}},
            False,
        ),
        (
            {"type": "dict", "key": "test", "validation": {"required": False}},
            False,
        ),
        (
            {
                "type": "dict",
                "key": "test",
                "value": [
                    {"type": "str", "key": "test", "validation": {"required": True}}
                ],
            },
            False,
        ),
        (
            {"type": "str", "key": "test", "validation": {"options": []}},
            False,
        ),
        (
            {"type": "str", "key": "test", "validation": {"options": [None]}},
            False,
        ),
        (
            {"type": "str", "key": "test", "validation": {"options": [1]}},
            False,
        ),
        (
            {"type": "bool", "key": "test", "validation": {"options": [True]}},
            False,
        ),
        (
            {
                "type": "str",
                "key": "test",
                "value": "test",
                "validation": {"options": ["test2"]},
            },
            False,
        ),
        (
            {
                "type": "str",
                "key": "test",
                "value": "test",
                "validation": {"options": ["test2"]},
            },
            True,
        ),
    ],
)
def test_extra_schema_error(data, is_template):
    """Test if the extra schema validates invalid extras correctly."""
    with pytest.raises(ValidationError):
        ExtraSchema(is_template=is_template).load(data)


def test_extras_field():
    """Test if the extras form field works correctly."""
    data = [
        {
            "type": "str",
            "key": "test",
            "value": None,
            "validation": {"required": True},
        }
    ]

    class _TestForm(KadiForm):
        test = ExtrasField(is_template=True)

    form = _TestForm(formdata=MultiDict({"test": [json.dumps(data)]}))

    assert form.validate()
    assert form.test.data == data

    class _TestForm(KadiForm):
        test = ExtrasField()

    form = _TestForm(formdata=MultiDict({"test": [json.dumps(data)]}))

    assert not form.validate()
    assert "Invalid extra metadata." in form.errors["test"]
    assert form.test._value() == [
        {
            "type": {"value": "str", "errors": []},
            "key": {"value": "test", "errors": []},
            "value": {"value": None, "errors": ["Value is required."]},
            "unit": {"value": None, "errors": []},
            "validation": {"value": {"required": True}, "errors": []},
        }
    ]
