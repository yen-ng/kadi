# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from tests.modules.utils import check_api_delete_subject_resource_role
from tests.utils import check_api_response


def test_delete_group(api_client, dummy_access_token, dummy_group):
    """Test the "api.delete_group" endpoint."""
    response = api_client(dummy_access_token).delete(
        url_for("api.delete_group", id=dummy_group.id)
    )

    check_api_response(response, status_code=204)
    assert dummy_group.state == "deleted"


def test_remove_group_member(
    api_client, dummy_access_token, dummy_group, dummy_user, new_user
):
    """Test the "api.remove_group_member" endpoint."""
    user = new_user()

    check_api_delete_subject_resource_role(
        api_client(dummy_access_token),
        url_for("api.remove_group_member", group_id=dummy_group.id, user_id=user.id),
        user,
        dummy_group,
        remove_creator_endpoint=url_for(
            "api.remove_group_member", group_id=dummy_group.id, user_id=dummy_user.id
        ),
    )
