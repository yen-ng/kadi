# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from click.testing import CliRunner

from kadi.cli.main import kadi
from kadi.version import __version__


def test_kadi_cli():
    """Test whether the basic Kadi CLI works."""
    runner = CliRunner()
    result = runner.invoke(kadi, ["--version"])

    assert result.exit_code == 0
    assert result.output == f"{__version__}\n"
