# Release history

## 0.17.0 (Unreleased)

**Additions**

* Added customization options to exclude certain information in the JSON and
  PDF exports.
* Added API endpoints to export records and collections in all existing
  formats.

**Changes**

* User information referring to a resource is now always excluded when using a
  scoped personal access token without `user.read` scope, even if the
  corresponding endpoint itself is usable.

**Fixes**

* Fixed markdown and CSV previews not working if a file's content was not
  detected as `plain/text`.

## 0.16.0 (2021-07-26)

**Additions**

* Added basic handling of potential multivalued user attributes retrieved via
  Shibboleth, configurable via the `multivalue_separator` configuration value.
* Added an additional button to the resource edit pages to save changes without
  leaving the edit page and added the submit buttons to the top as well.
* Added a separate section for update notes in the documentation.
* Added a direct selection of record files as links to all markdown fields.
* Added a dismissable prompt for changing the language to the browser's
  preferred one, if possible.
* Added a CLI command to list all existing search indices.

**Changes**

* Improved the record PDF export to properly deal with long text cells without
  using truncation and many other related visual improvements.
* Improved the full-text search for records, collections and groups to give
  higher priority to exact matches.
* Adjusted the plugin specifications for using custom preview data components
  and added a new hook for custom scripts.
* Various smaller UI improvements.
* Changed the existing search mappings for records, collections and groups in
  order to further improve exact matches and searches with short queries.
  Please see the relevant update notes in the documentation.
* Refactored parsing of query parameter flags to allow specifying both typical
  truthy and falsy values and adjusted the API documentation accordingly.
* The button to download all record files is now shown for all records with at
  least one file.
* Updated some of the basic server configuration templates. Please see the
  relevant update notes in the documentation.
* Added additional metadata to record and collection JSON export.

**Fixes**

* Fixed corner case of being unable to package multiple files of a record if
  they are all empty.

## 0.15.1 (2021-06-28)

**Fixes**

* Fixed the direct upload of files (i.e. drawings and workflows) not working in
  certain cases.

## 0.15.0 (2021-06-25)

**Additions**

* Added a new filter parameter to the group search API and UI to retrieve only
  groups with membership.
* Added sections and information about record links and collections to the
  record PDF export.
* Added a toggle to the record links graph visualization to enable or disable
  forces.
* Added shortcuts to the metadata viewer for jumping directly to the respective
  entry in the editor.
* Added persistent IDs to the record PDF export
* Added an SVG export functionality to the record links graph.
* Added previews for BMP, TIFF and GIF image files.

**Changes**

* Changed the functionality to retrieve a user's groups in the API and UI to
  retrieve either created or common groups.
* Use existing archives if possible when downloading multiple record files as
  long as they are still up to date.
* Replaced the `psycopg2-binary` Python package with `psycopg2`. Please see the
  relevant update notes in the documentation.
* Pinned all Python dependencies to ensure reproducible installations.
* Changed all of the divided resource views (e.g. the record overview page) to
  only load content once a corresponding tab is actually shown.

**Fixes**

* Fixed inconsistent undo/redo behaviour in the metadata editor.

## 0.14.0 (2021-06-07)

**Changes**

* Added success indicator when directly uploading drawings and workflows.
* Added indication for ports allowing multiple connections in experimental
  workflow editor.
* Added previews for some common multimedia formats.
* Show an overview of the currently used record template when creating new
  records.
* Added a direct selection of image record files to all markdown fields.
* Added additional allowed socket combinations and the ability to view them
  when hovering over a socket in experimental workflow editor.
* Added a new configuration value `ELASTICSEARCH_CONFIG` to further customize
  the Elasticsearch connection for more intricate setups.
* Adjusted the Elasticsearch configuration in the installation script and
  manual installation instructions for the described single-node setup. Please
  see the relevant update notes in the documentation.
* Updated the structure of the documentation as well as some installation and
  configuration sections.
* Added support for environment tools in experimental workflow functionality.

**Fixes**

* Fixed rounding error in canvas painter potentially causing slightly blurry or
  misplaced images.
* Fixed broken scrolling behaviour in experimental workflow editor for some
  OS/browser combinations.
* Replaced the broken `kadi utils secret-key` command with using `/dev/urandom`
  directly in the installation instructions and script.

## 0.13.0 (2021-05-20)

**Changes**

* Show current version in *About* page for authenticated users only.
* Adjusted priority of display name related attributes in LDAP configuration.
  If `firstname_attr` and `lastname_attr` are specified, they always take
  precedence over `displayname_attr`.
* Added a tabular preview for CSV files.
* Updated builtin user input nodes in experimental workflow editor.
* Included links to potential parent and child revisions in relevant API
  endpoints.
* Added a description field to templates.

**Fixes**

* Fixed preview for archives not working in some corner cases.
* Added missing metadata validation when creating record links via the API.
* Fixed search field in static selections also not receiving focus anymore.

## 0.12.0 (2021-04-23)

**Changes**

* Added a general `active_directory` flag for the LDAP authentication
  configuration, which also replaces the `supports_std_exop` flag.
* Improved the installation and configuration instructions in the
  documentation.
* Added a toggle to view the record metadata validation in the template
  overview.
* Excluded some data in the JSON export that is only relevant when using the
  API.
* Hide record template selection when copying records.

**Fixes**

* Fixed public shared user/group resources not appearing.
* Fixed LDAP authentication for Active Directories.
* Fixed some broken buttons when managing connected services or publishing
  records.
* Fixed preview for archives sometimes showing an incorrect folder structure.
* Fixed some buttons being visible without suitable permissions.
* Fixed search field in dynamic selections not receiving focus anymore.

## 0.11.0 (2021-04-14)

**Changes**

* Added a graph visualization for record links.
* Added some sections and revised many existing chapters in the documentation.
* Changed ordering of returned resources in record/collection link API
  endpoints.
* Updated builtin nodes in experimental workflow editor.

## 0.10.0 (2021-04-02)

**Changes**

* Updated some preview components and added a toggle to view the source text of
  markdown files.
* Renamed "Copy" functionality to "Duplicate" in metadata editor.
* Added a preview for STL model files.

**Fixes**

* Fixed `utils` commands potentially using the wrong path value for virtual
  environments.

## 0.9.0 (2021-03-24)

**Changes**

* Updated builtin nodes in experimental workflow editor.
* Reintroduced basic keyboard shortcuts to easily switch to the different
  resource overview pages.
* Added the ability to specify optional validation instructions for the generic
  record metadata. This can be done both in templates and the actual records
  and is mainly intended for use in the graphical metadata editor to facilitate
  the manual entry of metadata. In the first version, `required` and `options`
  are supported.

**Fixes**

* Fixed crash in record PDF export related to `null` date values.
* Fixed some dependency ports not accepting multiple connections in
  experimental workflow editor.

## 0.8.1 (2021-03-17)

**Changes**

* Added `/info` endpoint to API.

**Fixes**

* Removed broken keyboard shortcuts again.

## 0.8.0 (2021-03-16)

**Changes**

* Added record template selection to "New record" menu as well.
* Added some basic keyboard shortcuts to switch to the different resource
  overview pages.
* Adjusted content shown on home page.

**Fixes**

* Explicitely pinned SQLAlchemy version to `<1.4.0`.

## 0.7.1 (2021-03-11)

**Fixes**

* Fixed crash in record PDF export related to parsing certain dates.

## 0.7.0 (2021-03-10)

**Changes**

* Improved the user management sysadmin interface.
* Added the option to set the link direction when linking records.
* Added PDF export functionality to records.
* Major refactoring of experimental workflow editor and corresponding
  functionality.
* Added an option to reset the canvas size in the canvas painter.
* Improved help pages and added additional help popovers.
* Added additional filter functionality for resource permissions in the web
  interface and API.
* Major refactoring of record, collection and group search interfaces.
* Added collection filter to record search interface and API.

**Fixes**

* Hide "Update content" button in file overview if user lacks permission.
* Fixed broken filter functionality on some pages.
* Fixed permission checks for retrieving shared group resources via API.
* Fixed crash in extra metadata search in certain corner cases.

## 0.6.0 (2021-02-05)

**Changes**

* Added the first version of a sysadmin interface.
* Various smaller UI improvements.

**Fixes**

* Made the canvas painter work with touch devices.
* Restricted extra metadata integer values to values safe for parsing them in a
  JS context, e.g. in the metadata editor.
* Fixed API endpoint to get a file of a record by its name.
* Fixed creating orphaned revisions after deleting resources.

## 0.5.0 (2021-01-13)

**Changes**

* Made endpoints to manage the trash/deleted resources part of the public API.
  These endpoints require a new scope for any personal access token
  (`misc.manage_trash`).
* Added plugin hooks for specifying custom MIME types, preview data and
  components.
* Added a preview for Markdown files.
* Added functionality to filter out public resources in the searches.
* Added the option to copy a template, to apply it from the overview and
  improved exporting extra record metadata as template.
* Added a "QR Code" export for records and collections.
* Added new tabs to the group overview to view resources shared with a group.
* Added a canvas painter to create drawings and upload them as files to a
  record.
* Added functionality to edit previous drawings and other images.
* Added many improvements to the documentation.
* Various other smaller improvements.

**Fixes**

* Fixed another bug in the latest migration script that caused it not to run
  under certain circumstances.
* Fixed and improved MIME type detection based on file's contents.
* Various other smaller bug fixes.

## 0.4.3 (2020-12-07)

* Fixed a bug in the latest migration script that caused it not to run under
  certain circumstances.

## 0.4.2 (2020-12-07)

* Unified record types to be always lowercase.
* Fixed Zenodo plugin for records with too short descriptions.

## 0.4.1 (2020-12-04)

* Improved some error messages.

## 0.4.0 (2020-12-03)

* Added functionality to publish records using different providers, which can
  be registered as plugins.
* Added a Zenodo plugin, enabling users to connect their accounts and to
  publish records.
* Added option to specify a common bind user to use for LDAP operations in the
  LDAP authentication provider.
* Added additional user management commands to the CLI.
* Various smaller bug fixes and GUI improvements.

## 0.3.0 (2020-11-10)

* Allow exact matches for keys and string values in the extra metadata search
  by using double quotes.
* Improved LDAP authentication and added the option to allow LDAP users to
  change their password.
* Slightly improved plugin infrastructure and added additional hooks.
* Added a new page to the settings page, allowing users to manage connected
  services, which can be registered as plugins.
* Added a license field to records.
* Various smaller bug fixes and improvements.

## 0.2.0 (2020-10-02)

* Removed linking resources with groups. Group links did not add much value in
  their current form but rather lead to confusion. Something similar might be
  brought back in the future again.
* Added and improved some more translations.
* Migrate celery configuration to new format.
* Various smaller bug fixes and improvements.

## 0.1.2 (2020-09-22)

* Added an installation script for production environments and instructions.
* Some other updates to the documentation and configuration templates.

## 0.1.1 (2020-09-15)

* Cookies are now set only for the current domain.
* Small updates to the documentation and configuration templates.

## 0.1.0 (2020-09-14)

* Initial beta release.
